import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import NavBarNotLoggedIn from './components/navBarNotLoggedIn'
import Post from './components/post'
import PostNotLoggedIn from './components/postNotLoggedIn'
import { useParams, Redirect } from 'react-router-dom'


function UserProfile() {
    const userAuth = useSelector(state => state.auth.user)
    const [user, setUser] = useState({})
    const [postData, setPostData] = useState(null)
    let { id } = useParams();

    useEffect(() => {
        apiService.getUserById(id).then(user => {
            setUser(user);
        });
        apiService.getPostsForUserApi(id).then(data => {
            setPostData(data)
        });
    }, []);

    let UserName = userAuth ? userAuth.profile.name : null

    function followUser(followUserId) {
        apiService.followUserApi(UserName, followUserId)
    }

    return (UserName == id) ?
        (<Redirect to={'/profile'} />) :
        (<div>
            {UserName ? <NavBar /> : <NavBarNotLoggedIn />}
            <div className="container">
                <div className="row">
                    <div className="column column-offset-20">
                        <h3>{user.username}</h3>
                        <h5>Biography:</h5>
                        <p>{user.biography}</p>
                    </div>
                    <div className="column column-offset-20">
                        <h5>Verified: {user.isVerified ? "Yes" : "No"}</h5>
                        <h5>Website: {user.website}</h5>
                        {UserName ? <button onClick={() => followUser(user.username)} className='button button-outline'>Follow</button> : ""}
                    </div>
                </div>
                <div className="row">
                    <div className="column column-50 column-offset-25">
                        {UserName ? <Post posts={postData} /> : <PostNotLoggedIn posts={postData} />}
                    </div>
                </div>
            </div>
        </div>)
}

export default UserProfile;