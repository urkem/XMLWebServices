import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import Post from './components/post'
import NavBar from './components/navBar'
import { useForm } from "react-hook-form"


function Home() {
    const user = useSelector(state => state.auth.user)
    const [postData, setPostData] = useState(null)
    const { register, handleSubmit } = useForm();

    async function filterPosts(data) {
        const posts = apiService.filterPostsApi(data);
        setPostData(posts);
    }

    async function getDoughnuts() {
        const doughnuts = await apiService.getPostsFromFollowersApi(user.profile.name)
        setPostData(doughnuts)
    }

    useEffect(() => {
        getDoughnuts();
    }, []);


    const onSubmit = (data) => filterPosts(data);

  return (
      <div>
          <NavBar />
          <div className="container">
              <div className="row">
                  <div className="column column-50 column-offset-25">
                      <form onSubmit={handleSubmit(onSubmit)}>
                          <label htmlFor="filterValue">Filter</label>
                          <input type="text" {...register("filterValue")} placeholder="Filter" />
                          <select {...register("type")}>
                              <option value="tag">tag</option>
                          </select>
                          <hr />
                          <input type="submit" />
                      </form>
                      <Post posts={postData} />
                  </div>
              </div>
          </div>
    </div>
  )
}

export default Home
