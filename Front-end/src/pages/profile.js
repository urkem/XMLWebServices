import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import Post from './components/post'

function Profile() {
    const userAuth = useSelector(state => state.auth.user)
    const [user, setUser] = useState({})
    const [postData, setPostData] = useState(null)

    useEffect(() => {
        apiService.getUserById(userAuth.profile.name).then(user => {
            setUser(user);
        });
        apiService.getPostsForUserApi(userAuth.profile.name).then(data => {
            setPostData(data)
        });
    }, []);

    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <div className="column column-offset-20">
                        <h3>{userAuth.profile.name}</h3>
                        <h5>Biography:</h5>
                        <p>{user.biography}</p>
                    </div>
                    <div className="column column-offset-20">
                        <h5>Verified: {user.isVerified ? "Yes" : "No"}</h5>
                        <h5>Website: {user.website}</h5>
                    </div>
                </div>
                <div className="row">
                    <div className="column column-50 column-offset-25">
                        <Post posts={postData} />
                    </div>
                </div>
            </div>
        </div>)
}

export default Profile;