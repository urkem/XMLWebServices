import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import Post from './components/post'

function InteractedPosts() {
    const userAuth = useSelector(state => state.auth.user)
    const [postData, setPostData] = useState(null)

    useEffect(() => {
        apiService.getAllInteractedPostsApi().then(data => {
            setPostData(data)
        });
    }, []);

    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <div className="column column-50 column-offset-25">
                        <Post posts={postData} />
                    </div>
                </div>
            </div>
        </div>)
}

export default InteractedPosts;