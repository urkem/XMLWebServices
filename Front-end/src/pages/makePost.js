import React from 'react'
import { useForm } from "react-hook-form"
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import { useHistory } from 'react-router-dom'

function MakePost() {
    const userAuth = useSelector(state => state.auth.user)
    const { register, handleSubmit } = useForm();
    const history = useHistory();

    const onSubmit = (data) => apiService.sendMessageApi(userAuth.profile.name, data).then(res => {
        history.push('/');
    });

    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <div className="column column-50 column-offset-25">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <label htmlFor="description">Description</label>
                            <input type="text" {...register("description")} placeholder="description" />
                            <label htmlFor="files">Image</label>
                            <input type="file" {...register("files")}/>
                            <hr/>
                            <input type="submit" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MakePost;