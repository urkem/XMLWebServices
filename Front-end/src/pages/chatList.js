import React, { useState, useEffect } from 'react'
import * as apiService from '../services/apiService'
import { useHistory } from 'react-router-dom'
import NavBar from './components/navBar'

function ChatList() {
    const [chatList, setChatList] = useState([]);
    const history = useHistory();

    function goToChat(user) {
        history.push("/chat/" + user)
    }

    useEffect(() => {
        apiService.getAllChatsApi().then(data => {
            let chatListTmp = data ? data.map((value, index) => {
                return (
                    <div key={index}>
                        <br/>
                        <h2 onClick={() => goToChat(value)}>{value}</h2>
                    </div>)
            }) : null;
            setChatList(chatListTmp);
        });
    }, []);

    return (
        <div>
        <NavBar/>
        <div className="container">
            <div className="row">
                <div className="column column-50 column-offset-25">
                    {chatList}
                </div>
            </div>
            </div>
        </div>)
}

export default ChatList