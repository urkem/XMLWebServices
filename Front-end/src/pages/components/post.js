import React from 'react'
import * as apiService from '../../services/apiService'
import { useHistory } from 'react-router-dom'


function Post(props) {
    const history = useHistory()

    function goToUserProfile(userId){
        //console.log(userId);
        history.push("/user/" + userId)
    }


    const postsList = props.posts ? props.posts.map((value,index) => {
        return <div key={index}>
            <h4 onClick={() => goToUserProfile(value.userId)}>{value.userId}</h4>
            {value.urls ? value.urls.map((urls, index) => { return <img src={urls} key={index} /> })
                : "No img"}
            <br />
            <button className="button button-outline" onClick={() => apiService.reactPostApi(value.id, true)}>Like</button>
            <button className="button button-outline" onClick={() => apiService.reactPostApi(value.id, false)}>Dislike</button>
            <p>
                Description :{value.description} {value.description}<br />
                Likes :{value.numberOfLikes} Dislikes :{value.numberOfDislikes}
            </p>
        </div>
    }) : console.log(props)

    return (
        <div>
            {postsList}
        </div>
    )
}

export default Post