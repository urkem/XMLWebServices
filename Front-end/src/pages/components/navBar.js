import React from 'react'
import { Link } from 'react-router-dom'
import { signoutRedirect } from '../../services/userService'


function NavBar() {
    return (<nav>
        <Link to="/">Home</Link>
        <Link to="/profile">Profile</Link>
        <Link to="/edit-profile">Edit profile</Link>
        <Link to="/make-post">Make post</Link>
        <Link to="/interacted-posts">Interacted posts</Link>
        <Link to="/chats">Chat</Link>
        <Link to="/notifications">Notifications</Link>
        <a onClick={() => signoutRedirect()}>Sign Out</a>
    </nav>)
}

export default NavBar;