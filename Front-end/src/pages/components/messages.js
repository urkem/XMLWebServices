import React from 'react'
import { useSelector } from 'react-redux'

function Messages(props) {
    const userAuth = useSelector(state => state.auth.user);

    const msgList = props.msg ? props.msg.map((value, index) => {
        return (
        <div key={index}>
            <p className={userAuth.profile.name == value.userSender1Id ? "pushLeft" : "pushRight"}>
                  {value.text}
            </p>
        </div>)
        
    }) : console.log(props)

    return (
        <div className="column">
            {msgList}
        </div>
    )
}

export default Messages