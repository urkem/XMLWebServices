import React from 'react'
import { useHistory } from 'react-router-dom'

function PostNotLoggedIn(props) {
    const history = useHistory()

    function goToUserProfile(userId){
        console.log(userId);
        history.push("/user/" + userId)
    }


    const postsList = props.posts ? props.posts.map((value,index) => {
        return <div key={index}>
            <h4 onClick={() => goToUserProfile(value.userId)}>{value.userId}</h4>
            {value.urls ? value.urls.map((urls, index) => { return <img src={urls} key={index} /> })
                : "No img"}
            <br />
            <p>
                Description :{value.description} <br />
                Likes :{value.numberOfLikes} Dislikes :{value.numberOfDislikes}
            </p>
        </div>
    }) : console.log(props)

    return (
        <div>
            {postsList}
        </div>
    )
}

export default PostNotLoggedIn