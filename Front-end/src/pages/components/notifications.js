import React from 'react'
import * as apiService from '../../services/apiService'
import { useHistory } from 'react-router-dom'


function Notification(props) {
    const history = useHistory()

    function goToUserProfile(userId) {
        history.push("/user/" + userId)
    }

    const notifiList = props.notifi ? props.notifi.map((value, index) => {
        return <div key={index}>
            <h5 onClick={() => goToUserProfile(value.userNotify)}>{value.userNotify}</h5>
            <p><b>{value.contentType}: </b>{value.content}</p><p>{value.timeOfSending}</p>
        </div>
    }) : console.log(props)

    return (
        <div className="column">
            {notifiList}
        </div>
    )
}

export default Notification