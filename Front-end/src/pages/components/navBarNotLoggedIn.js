import React from 'react'
import { Link } from 'react-router-dom'
import { signinRedirect } from '../../services/userService'


function NavBarNotLoggedIn() {
    return (<nav>
        <Link to="/login">Home</Link>
        <a onClick={() => signinRedirect()}>Sign in</a>
    </nav>)
}

export default NavBarNotLoggedIn;