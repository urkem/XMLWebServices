import React, { useState, useEffect } from 'react'
import { useForm } from "react-hook-form"
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import { Link, useHistory  } from 'react-router-dom'


function EditProfile() {
    const userAuth = useSelector(state => state.auth.user)
    const { register, handleSubmit, setValue } = useForm();
    const history = useHistory();
    const onSubmit = (data) => apiService.updateUser(userAuth.profile.name, data).then(res => {
        //console.log(res);
        history.push('/')
    });

    useEffect(() => {
        // get user and set form fields
        apiService.getUserById(userAuth.profile.name).then(user => {
            const fields = ['firstName', 'lastName', 'email', 'phoneNumber',
                'gender', 'dateOfBirth', 'website', 'biography', 'isPrivate',
                'receiveMsgFromUnfollowed', 'isTagable', 'isVerified', 'followNotifications',
                'msgNotifications', 'postNotifications', 'commentNotifications'];
            fields.forEach(field => setValue(field, user[field]));
        });
    }, []);

    return (
        <div>
        <NavBar/>
        <div className="container">
            <div className="row">
                <div className="column column-50 column-offset-25">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" {...register("firstName")} placeholder="First name" />
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" {...register("lastName")} placeholder="Last name" />
                        <label htmlFor="email">Email</label>
                        <input type="email" {...register("email")} placeholder="Email" />
                        <label htmlFor="phoneNumber">Phone Number</label>
                        <input type="text" {...register("phoneNumber")} placeholder="Phone Number" />
                        <label htmlFor="gender">Gender</label>
                        <select {...register("gender")}>
                            <option value="">Select...</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                        <label htmlFor="dateOfBirth">Date Of Birth</label>
                        <input type="datetime-local" {...register("dateOfBirth")} placeholder="Date Of Birth" />
                        <label htmlFor="website">Website</label>
                        <input type="text" {...register("website")} placeholder="website" />
                        <label htmlFor="biography">Biography</label>
                        <input type="text" {...register("biography")} placeholder="Biography" />
                        <label htmlFor="isPrivate">Private</label>
                        <input type="checkbox" {...register("isPrivate")}/>
                        <label htmlFor="receiveMsgFromUnfollowed">Receive Msg From Unfollowed</label>
                        <input type="checkbox" {...register("receiveMsgFromUnfollowed")}/>
                        <label htmlFor="isTagable">Tagable</label>
                        <input type="checkbox" {...register("isTagable")} />
                        <label htmlFor="isVerified">Varified</label>
                        <input type="checkbox" {...register("isVerified")} disabled />
                        <Link to="/verify" className="button"> verify</Link>
                        <hr />
                        <h4>Notification Settings</h4>
                        <label htmlFor="followNotifications">Follow Notifications</label>
                        <input type="checkbox" {...register("followNotifications")}/>
                        <label htmlFor="msgNotifications">Msg Notifications</label>
                        <input type="checkbox" {...register("msgNotifications")} />
                        <label htmlFor="postNotifications">Post Notifications</label>
                        <input type="checkbox" {...register("postNotifications")} />
                        <label htmlFor="commentNotifications">Comment Notifications</label>
                        <input type="checkbox" {...register("commentNotifications")} />
                        <hr/>
                        <input type="submit" />
                    </form>
                </div>
            </div>
        </div>
        </div>
    );
}

export default EditProfile;