import React, { useState, useEffect } from 'react'
import { useForm } from "react-hook-form"
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import { useParams } from 'react-router-dom'
import Messages from './components/messages'



function ChatWithUser() {
    const userAuth = useSelector(state => state.auth.user)
    const { register, handleSubmit } = useForm();
    const [msgData, setMsgData] = useState(null)
    let { id } = useParams();


    const onSubmit = (data) => apiService.sendMessageApi(id, data).then(res => {
        console.log(res);
    });

    useEffect(() => {
        apiService.getChatHistoryApi(id).then(data => {
            setMsgData(data);
        });
    }, []);


    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <Messages msg={msgData} userName={id} />
                </div>
                <div className="row">
                    <div className="column column-offset-15">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <input type="text" {...register("msg")} placeholder="msg" />
                            <input type="submit" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ChatWithUser;