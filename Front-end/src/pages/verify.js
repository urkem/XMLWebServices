import React from 'react'
import { useForm } from "react-hook-form"
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import NavBar from './components/navBar'
import { useHistory } from 'react-router-dom'

function Verify() {
    const userAuth = useSelector(state => state.auth.user)
    const { register, handleSubmit } = useForm();
    const history = useHistory();

    const onSubmit = (data) => apiService.sendVerifyApi(userAuth.profile.name, data).then(res => {
        history.push('/');
    });

    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <div className="column column-50 column-offset-25">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <label htmlFor="firstname">First name</label>
                            <input type="text" {...register("firstname")} placeholder="firstname" />
                            <label htmlFor="lastname">First name</label>
                            <input type="text" {...register("lastname")} placeholder="lastname" />
                            <label htmlFor="category">First name</label>
                            <input type="text" {...register("category")} placeholder="category" />
                            <label htmlFor="files">Document</label>
                            <input type="file" {...register("files")} />
                            <hr />
                            <input type="submit" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Verify;