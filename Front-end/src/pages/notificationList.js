import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import * as apiService from '../services/apiService'
import Notification from './components/notifications'
import NavBar from './components/navBar'

function NotificationList() {
    const user = useSelector(state => state.auth.user)
    const [notifiData, setNotifiData] = useState(null)

    async function getNotifications() {
        const doughnuts = await apiService.getNotificationsApi(user.profile.name);
        setNotifiData(doughnuts);
    }

    useEffect(() => {
        getNotifications();
    }, []);

    return (
        <div>
            <NavBar />
            <div className="container">
                <div className="row">
                    <Notification notifi={notifiData} />
                </div>
            </div>
        </div>
    )
}

export default NotificationList
