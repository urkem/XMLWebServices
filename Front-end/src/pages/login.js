import React, { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import NavBarNotLoggedIn from './components/navBarNotLoggedIn'
import PostNotLoggedIn from './components/postNotLoggedIn'
import * as apiService from '../services/apiService'

function Login() {
    const user = useSelector(state => state.auth.user)
    const [postData, setPostData] = useState(null)

    async function getDoughnuts() {
        const doughnuts = await apiService.getDoughnutsFromApi()
        setPostData(doughnuts)
    }

    useEffect(() => {
        getDoughnuts();
    }, []);

  return (
    (user) ?
      (<Redirect to={'/'} />)
      :
      (<div>
              <NavBarNotLoggedIn />
              <div className="container">
                  <div className="row">
                      <div className="column column-50 column-offset-25">
                          <PostNotLoggedIn posts={postData} />
                      </div>
                  </div>
              </div>
      </div>)
  )
}

export default Login
