import axios from 'axios'

async function getDoughnutsFromApi() {
    const response = await axios.get('https://localhost:5006/Posts/GetAllPosts');
    return response.data;
}

async function reactPostApi(postId, isLiked) {
    return await axios.post(`https://localhost:5006/Posts/React?Id=${postId}&IsLiked=${isLiked}`);
}

async function getUserById(userName) {
    const response = await axios.get("https://localhost:5001/Users/" + userName);
    return response.data;
}

async function updateUser(userName, data) {
    return await axios.put("https://localhost:5001/Users/" + userName, data)
}

async function makePostApi(userId, data) {
    var bodyFormData = new FormData();
    bodyFormData.append("userId", userId);
    bodyFormData.append("description", data.description);
    bodyFormData.append("files", data.files[0]);
    return await axios.post("https://localhost:5006/Posts/Upload", bodyFormData);
}

async function getPostsForUserApi(userId) {
    const response = await axios.get('https://localhost:5006/Posts/GetPostsByUser/' + userId);
    return response.data;
}

async function sendMessageApi(receverUserName, data) {
    return await axios.post(`http://localhost:5012/SendMessage?recieverId=${receverUserName}&text=${data.msg}`)
}

async function getAllInteractedPostsApi() {
    const response = await axios.get('https://localhost:5006/Posts/GetPostsWithReactionByUser');
    return response.data;
}

async function getAllChatsApi() {
    const response = await axios.get("http://localhost:5012/GetAllFriendsThatChatted");
    return response.data;
}

async function getChatHistoryApi(reciverUserName) {
    const response = await axios.get('http://localhost:5012/Messages/Messages/GetHistoryChat?recieverId='+ reciverUserName);
    return response.data;
}

async function getNotificationsApi(reciverUserName) {
    const response = await axios.get("http://localhost:5014/Notification/GetPostsByUser/"+ reciverUserName);
    return response.data;
}

async function sendVerifyApi(userId, data) {
    var bodyFormData = new FormData();
    bodyFormData.append("userId", userId);
    bodyFormData.append("firstname", data.firstname);
    bodyFormData.append("lastname", data.lastname);
    bodyFormData.append("category", data.category);
    bodyFormData.append("files", data.files[0]);
    return await axios.post("https://localhost:5006/Admin/Upload", bodyFormData);
}

async function getPostsFromFollowersApi(userId, ) {
    const response = await axios.get('https://localhost:5006/Followers/Followers/GetFollowers/' + userId);
    var bodyFormData = new FormData();
    bodyFormData.append("followedUsers", response.data);
    const response2 = await axios.post('https://localhost:5006/Posts/GetPostsByFollowedUsers?daysAgo=30', bodyFormData);
    return response2.data;
}

async function followUserApi(UserName, followUserId) {
    return await axios.post(`https://localhost:5006/Followers/Followers/CreateFollow/${UserName}/${followUserId}`)
}

async function filterPostsApi(data) {
    const response = await axios.get("https://localhost:5006/Posts/SearchPostsByTag/" + data.filterValue);
    return response.data;
}

export {
    getDoughnutsFromApi,
    reactPostApi,
    getUserById,
    updateUser,
    makePostApi,
    getPostsForUserApi,
    getAllInteractedPostsApi,
    sendMessageApi,
    getAllChatsApi,
    getChatHistoryApi,
    getNotificationsApi,
    sendVerifyApi,
    getPostsFromFollowersApi,
    followUserApi,
    filterPostsApi
}
