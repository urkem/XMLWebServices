import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import SigninOidc from './pages/signin-oidc'
import SignoutOidc from './pages/signout-oidc'
import Home from './pages/home'
import Login from './pages/login'
import EditProfile from './pages/editProfile'
import MakePost from './pages/makePost'
import Profile from './pages/profile'
import UserProfile from './pages/userProfile'
import InteractedPosts from './pages/InteractedPosts'
import ChatWithUser from './pages/chat'
import ChatList from './pages/chatList'
import NotificationList from './pages/notificationList'
import Verify from './pages/verify'
import { Provider } from 'react-redux';
import store from './store';
import userManager, { loadUserFromStorage } from './services/userService'
import AuthProvider from './utils/authProvider'
import PrivateRoute from './utils/protectedRoute'

function App() {

  useEffect(() => {
    // fetch current user from cookies <Route path="/user/:id" component={UserProfile} />
    loadUserFromStorage(store)
  }, [])

  return (
    <Provider store={store}>
      <AuthProvider userManager={userManager} store={store}>
        <Router>
          <Switch>
            <Route path="/login" component={Login} />
            <Route path="/signout-oidc" component={SignoutOidc} />
            <Route path="/signin-oidc" component={SigninOidc} />
            <Route path="/user/:id" component={UserProfile} />
            <PrivateRoute exact path="/" component={Home} />
            <PrivateRoute path="/profile" component={Profile} />
            <PrivateRoute path="/edit-profile" component={EditProfile} />
            <PrivateRoute path="/make-post" component={MakePost} />
            <PrivateRoute path="/interacted-posts" component={InteractedPosts} />
            <PrivateRoute path="/chat/:id" component={ChatWithUser} />
            <PrivateRoute path="/chats" component={ChatList} />
            <PrivateRoute path="/notifications" component={NotificationList} />
            <PrivateRoute path="/verify" component={Verify} />
          </Switch>
        </Router>
      </AuthProvider>
    </Provider>
  );
}

export default App;
