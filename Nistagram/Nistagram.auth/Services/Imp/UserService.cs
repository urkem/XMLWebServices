﻿using Nistagram.auth.Data;
using Nistagram.auth.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.auth.Services.Imp
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;

        public UserService(ApplicationDbContext context)
        {
            _context = context;

        }

        public void BlockUser(string userWhoBlocksId, string blockedUserId)
        {
            ApplicationUser userWhoBlocks = _context.Users.Find(userWhoBlocksId);
            ApplicationUser blockedUser = _context.Users.Find(blockedUserId);
            if (!userWhoBlocks.BlockedUsers.Contains(blockedUser))
            {
                userWhoBlocks.BlockedUsers.Add(blockedUser);
                _context.Update(userWhoBlocks);
                _context.SaveChanges();
            }
        }
    }
}
