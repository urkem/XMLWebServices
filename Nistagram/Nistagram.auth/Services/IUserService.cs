﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.auth.Services
{
    public interface IUserService
    {
        public void BlockUser(string userWhoBlocksId, string blockedUserId);
    }
}
