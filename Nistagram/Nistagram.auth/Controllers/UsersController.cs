﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Nistagram.auth.Entities;
using Nistagram.auth.Services;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static IdentityServer4.IdentityServerConstants;

namespace Nistagram.auth.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IUserService _userservice;

        public UsersController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IUserService userService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _userservice = userService;
        }

        [HttpGet]
        //[Authorize(Policy = LocalApi.PolicyName)]
        public ActionResult<IEnumerable<UserDTO>> Get()
        {
            // test var roles = _roleManager.Roles.ToList();
            return Ok(_userManager.Users.ToList().Select(x => x.AsDto()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(string id)
        {
            var user = await _userManager.FindByNameAsync(id);

            if (user is null)
            {
                return NotFound();
            }

            return Ok(user.AsDto());
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(string id, [FromBody] UpdateUserDto userDto)
        {
            var user = await _userManager.FindByNameAsync(id);

            if (user is null)
            {
                return NotFound();
            }

            //user.Email = userDto.Email;
            //user.UserName = userDto.Email;
            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            user.Address = userDto.Address;
            user.Country = userDto.Country;
            user.PhoneNumber = userDto.PhoneNumber;
            user.Gender = userDto.Gender;
            user.DateOfBirth = userDto.DateOfBirth;
            user.Website = userDto.Website;
            user.Biography = userDto.Biography;

            if(user.IsPrivate != userDto.IsPrivate)
            {
                //TODO: test
                //[Route("[controller]/[action]/{userId}/{privacy}")]
                //public async Task<IActionResult> ChangePostsPrivacy(string userId, bool isPrivate)
                RestClient restClient =
                    new RestClient(string.Format("{0}/Posts/ChangePostsPrivacy/{1}/{2}", "https://nistagram.services.posts:443", user.Email, userDto.IsPrivate));
                RestRequest request = new RestRequest(Method.POST);
                request.RequestFormat = DataFormat.Json;
                //request.AddJsonBody(data); //data is C# model for post request
                //request.AddHeader("Accept", "application/pdf");
                var result = restClient.Execute(request);
            }

            user.IsPrivate = userDto.IsPrivate;
            user.ReceiveMsgFromUnfollowed = userDto.ReceiveMsgFromUnfollowed;
            user.IsTagable = userDto.IsTagable;
            user.IsVerified = userDto.IsVerified;
            user.FollowNotifications = userDto.FollowNotifications;
            user.MsgNotifications = userDto.MsgNotifications;
            user.PostNotifications = userDto.PostNotifications;
            user.CommentNotifications = userDto.CommentNotifications;

            await _userManager.UpdateAsync(user);

            return NoContent();
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public bool IsUserPrivate(string userId)
        {
            return _userManager
                .Users
                .Where(x => x.Email == userId)
                .FirstOrDefault().IsPrivate;
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var user = await _userManager.FindByNameAsync(id);

            if (user is null)
            {
                return NotFound();
            }

            await _userManager.DeleteAsync(user);

            return NoContent();
        }

        [HttpPut]
        [Route("[action]/{userToBlockId}/")]
        public void BlockUser(string  userToBlockId)
        {
            var userNameId = User.FindFirstValue("name");
            _userservice.BlockUser(userNameId, userToBlockId);

        }
    }
}
