﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.auth.Entities
{
    public static class Extensions
    {
        public static UserDTO AsDto(this ApplicationUser user)
        {
            return new UserDTO(
                user.Email, 
                user.UserName,
                user.FirstName,
                user.LastName,
                user.PhoneNumber, 
                user.Address,
                user.Country, 
                user.Gender, 
                user.DateOfBirth,
                user.Website,
                user.Biography,
                user.IsPrivate,
                user.ReceiveMsgFromUnfollowed,
                user.IsTagable,
                user.IsVerified,
                user.FollowNotifications,
                user.MsgNotifications,
                user.PostNotifications,
                user.CommentNotifications);
        }
    }
}
