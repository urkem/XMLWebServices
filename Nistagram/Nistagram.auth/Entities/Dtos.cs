﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.auth.Entities
{
    public record UserDTO
    (
        string Email,
        string Username,
        string FirstName,
        string LastName,
        string PhoneNumber,
        string Address,
        string Country,
        string Gender,
        DateTime DateOfBirth,
        string Website,
        string Biography,
        bool IsPrivate,
        bool ReceiveMsgFromUnfollowed,
        bool IsTagable,
        bool IsVerified,
        bool FollowNotifications,
        bool MsgNotifications,
        bool PostNotifications,
        bool CommentNotifications
    );

    public record UpdateUserDto(
        [Required][EmailAddress]
        string Email,
        string FirstName,
        string LastName,
        string PhoneNumber,
        string Address,
        string Country,
        string Gender,
        DateTime DateOfBirth,
        string Website,
        string Biography,
        bool IsPrivate,
        bool ReceiveMsgFromUnfollowed,
        bool IsTagable,
        bool IsVerified,
        bool FollowNotifications,
        bool MsgNotifications,
        bool PostNotifications,
        bool CommentNotifications
    );
}
