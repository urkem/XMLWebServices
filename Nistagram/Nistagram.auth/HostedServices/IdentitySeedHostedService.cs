﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Nistagram.auth.Entities;
using Nistagram.auth.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nistagram.auth.HostedServices
{
    public class IdentitySeedHostedService : IHostedService
    {
        private readonly IServiceScopeFactory serviceScopeFactory;

        public IdentitySeedHostedService(IServiceScopeFactory serviceScopeFactory)
        {
            this.serviceScopeFactory = serviceScopeFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = serviceScopeFactory.CreateScope();

            var roleManagar = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();

            await CreateRoleIfNotExistsAsync(Roles.Admin, roleManagar);
            await CreateRoleIfNotExistsAsync(Roles.User, roleManagar);
            await CreateRoleIfNotExistsAsync(Roles.Agent, roleManagar);
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

        private static async Task CreateRoleIfNotExistsAsync(string role, RoleManager<IdentityRole> roleManager)
        {
            var roleExits = await roleManager.RoleExistsAsync(role);

            if (!roleExits)
                await roleManager.CreateAsync(new IdentityRole { Name = role });
        }
    }
}
