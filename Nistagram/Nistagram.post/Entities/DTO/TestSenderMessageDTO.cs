﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.post.Entities.DTO
{
    public record TestSenderMessageDTO(Guid Id, string Message);
}
