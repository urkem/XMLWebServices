﻿using Nistagram.post.Data.DTO;
using Nistagram.post.Entities.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.post.Entities
{

    [Table("tbTestSenders")]
    public class TestSender
    {
        public TestSender()
        {

        }

        public TestSender(TestSenderDTO testSenderDto)
        {
            this.Id = Guid.NewGuid();
            this.Message = testSenderDto.message;
        }

        public TestSender(TestSenderMessageDTO testSenderDto)
        {
            Id = testSenderDto.Id;
            Message = testSenderDto.Message;
        }

        [Key]
        public Guid Id { get; set; }


        public string Message { get; set; }

    }
}
