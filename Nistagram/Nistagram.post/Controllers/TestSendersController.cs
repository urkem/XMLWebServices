﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Nistagram.post.Data;
using Nistagram.post.Entities;
using Nistagram.post.Entities.DTO;

namespace Nistagram.post.Controllers
{
    
    [Route("[controller]")]
    [ApiController]
    public class TestSendersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TestSendersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TestSenders
        [HttpGet]
        public async Task<IEnumerable<TestSenderMessageDTO>> Get()
        {
            return (await _context.tbTestSenders.ToListAsync()).Select(x => new TestSenderMessageDTO(x.Id, x.Message)).ToList();
        }

        // GET: TestSenders/5
        [HttpGet("{id}")]
        public async Task<TestSenderMessageDTO> GetByIdAsync(Guid? id)
        {
            TestSender testSender = await _context.tbTestSenders.FindAsync(id);
            return new TestSenderMessageDTO(testSender.Id, testSender.Message);
        }
    }
}
