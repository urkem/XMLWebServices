﻿using Nistagram.post.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.post.Messaging
{
    interface ITestSenderNotifier
    {
        void SendTestSender(TestSenderDTO testSenderNotification);
    }
}
