﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Nistagram.post.Data.DTO;
using Nistagram.post.Messaging.Messages;

namespace Nistagram.post.Messaging.Impl
{
    public class TestSenderNotifier : ITestSenderNotifier
    {
        private readonly IRabbitManager _rabbitManager;
        private ILogger _logger;

        public TestSenderNotifier(IRabbitManager rabbitManager, ILoggerFactory loggerFactory)
        {
            _rabbitManager = rabbitManager;
            _logger = loggerFactory.CreateLogger<TestSenderNotifier>();
        }

        public void SendTestSender(TestSenderDTO testSenderNotification)
        {
            TestSenderMessage testSenderMessage = new TestSenderMessage(testSenderNotification);

            _logger.LogInformation($"Sending TestSenderMessage {testSenderMessage}");

            _rabbitManager.Publish(testSenderMessage, "testSender", "testSender.send");
        }
    }
}
