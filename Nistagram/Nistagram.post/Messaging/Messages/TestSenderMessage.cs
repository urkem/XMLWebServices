﻿using Nistagram.post.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.post.Messaging.Messages
{

    public class TestSenderMessage
    {
        public TestSenderMessage(TestSenderDTO testSenderNotification)
        {
            Id = testSenderNotification.Id;
            Message = testSenderNotification.message;
        }

        public Guid Id { get; set; }
        private string Message { get; set; }
    }
}
