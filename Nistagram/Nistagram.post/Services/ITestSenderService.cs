﻿using Nistagram.post.Data.DTO;
using System;

namespace Nistagram.post.Services
{
    public interface ITestSenderService
    {
        TestSenderDTO DoSomething(Guid id, string message);
    }
}