﻿
using Nistagram.post.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.post.Services.Impl
{
    public class TestSenderService : ITestSenderService
    {
        public TestSenderDTO DoSomething(Guid id, string message)
        {
            return new TestSenderDTO(id, message + " " + message);
        }
    }
}
