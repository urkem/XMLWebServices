﻿using Nistagram.Services.Notifications.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data
{
    public class NotificationDTO
    {
        public NotificationDTO()
        {
        }


        public NotificationDTO(Notification o)
        {
            this.UserId = o.UserId;
            this.UserNotify = o.UserNotify;
            this.ContentType = o.ContentType;
            this.Content = o.Content;
            this.TimeOfSending = o.TimeOfSending;
        }

        public string UserId { get; set; }
        public string UserNotify { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public DateTime TimeOfSending { get; set; }
    }
}
