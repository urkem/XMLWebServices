﻿using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Data.Model
{
    [Table("tbNotifications")]
    public class Notification
    {
        public Notification()
        {
        }

        public Notification(NotificationDTO notification)
        {
            this.Id = Guid.NewGuid();
            this.UserId = notification.UserId;
            this.UserNotify = notification.UserNotify;
            this.ContentType = notification.ContentType;
            this.Content = notification.Content;
            this.TimeOfSending = notification.TimeOfSending;

        }

        [Key]
        public Guid Id {get; set; }
        // User who was notified
        public string UserId { get; set; }
        // User who notified
        public string UserNotify { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public DateTime TimeOfSending { get; set; }


    }
}
