﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Notifications.Services;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Controllers
{
    [ApiController]
    public class NotificationController : Controller
    {
        private readonly ILogger<NotificationController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly INotificationService _notificationService;

        public NotificationController(ILogger<NotificationController> logger, INotificationService notificationService, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
            _notificationService = notificationService;
        }

        [HttpGet]
        [Route("[controller]/[action]/{profileUserId}")]
        public List<NotificationDTO> GetPostsByUser(string profileUserId)
        {
            //var profileUserId = User.FindFirstValue("name");
            return _notificationService.GetNotificationsByUser(profileUserId);
        }
    }
}
