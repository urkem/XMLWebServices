﻿using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Messaging
{
    interface ITestSenderNotifier
    {
        void SendTestSender(NotificationDTO testSenderNotification);
    }
}
