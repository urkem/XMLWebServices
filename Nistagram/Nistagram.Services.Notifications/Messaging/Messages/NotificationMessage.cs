﻿using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Messaging.Messages
{

    public class NotificationMessage
    {
        public NotificationMessage(NotificationDTO notification)
        {
            this.UserId = notification.UserId;
            this.UserNotify = notification.UserNotify;
            this.ContentType = notification.ContentType;
            this.Content = notification.Content;
            this.TimeOfSending = notification.TimeOfSending;
        }
        public string UserId { get; set; }
        public string UserNotify { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public DateTime TimeOfSending { get; set; }
    }
}
