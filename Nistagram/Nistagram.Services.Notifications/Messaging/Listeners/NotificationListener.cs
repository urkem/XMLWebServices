﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagram.Services.Notifications.Data.Model;
using Nistagram.Services.Notifications.Messaging.Context;
using Nistagram.Services.Notifications.Services;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Messaging.Listeners
{
    public class NotificationListener : ConsumerRabbit
    {
        private ILogger _logger;

        private INotificationService _testSenderService;

        private IServiceScopeFactory _serviceScopeFactory;


        public NotificationListener(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory) : base(loggerFactory, "testSender", "testSender.send")
        {
            _logger = loggerFactory.CreateLogger<NotificationListener>();
            _serviceScopeFactory = serviceScopeFactory;
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                _testSenderService = (INotificationService)scope.ServiceProvider.GetService(typeof(INotificationService));
                //_context = (ApplicationDbContext)scope.ServiceProvider.GetService(typeof(ApplicationDbContext));
                
            }
            //_context = context; 
        }

        protected override void HandleMessage(string content)
        {
            _logger.LogInformation($"Message received for UserCreatedMessage {content}");
            var createdUser = JsonConvert.DeserializeObject<NotificationDTO>(content);
            Notification notification = new Notification(createdUser);

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                dbContext.Notifications.Add(notification);
                dbContext.SaveChanges();

            }
            //_testSenderService.Save(notification);
            //_testSenderService.DoSomething(createdUser.Id, createdUser.message);
        }
    }
}
