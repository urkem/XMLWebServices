﻿
using Nistagram.Services.Notifications.Data.Model;
using Nistagram.Services.Notifications.Services;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Services.Impl
{
    public class NotificationService : INotificationService
    {
        private readonly ApplicationDbContext _context;

        public NotificationService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<NotificationDTO> GetNotificationsByUser(string profileUserId)
        {
            return _context.Notifications.Where(x => x.UserId == profileUserId).OrderBy(x => x.TimeOfSending)
                                    .Select(o => new NotificationDTO(o))
                                    .Distinct()
                                    .ToList();
        }

        public async Task Save(Notification notification)
        {
            _context.Notifications.Add(notification);
            await _context.SaveChangesAsync();

            //return Ok();
            //throw new NotImplementedException();
        }
    }
}
