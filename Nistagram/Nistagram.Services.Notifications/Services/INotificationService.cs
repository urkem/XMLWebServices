﻿using Nistagram.Services.Notifications.Data.Model;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Nistagram.Services.Notifications.Services
{
    public interface INotificationService
    {
        public Task Save(Notification notification);
        List<NotificationDTO> GetNotificationsByUser(string profileUserId);
    }
}