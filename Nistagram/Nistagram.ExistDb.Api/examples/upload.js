var Connection = require("../index.js");

var options = {
    host: "localhost",
    port: 8443,
    rest: "/exist/rest",
    auth: "admin:"
};
var connection = new Connection(options);
//console.log(encodeURI("C%%Users%Aleksa%Desktop%test.xml"))
// %db%apps%doc%test.xml
//%7B%22name%22:%22John%22,%20%22age%22:30,%20%22car%22:null%7D

const http = require("http");
const querystring = require('querystring');
const PORT = process.env.PORT || 5000;
const server = http.createServer(async (req, res) => {

    uriParts = req.url.split('/')
    console.log(uriParts)
    param = null
    isApi = false
    if(uriParts[1] == 'api')
        isApi = true

    if(uriParts.length > 2)
    {
        param = uriParts[2]
        param = param.replace("~~~",":/")
        console.log(param + " V1")
        param = param.split("~").join("/")
        console.log(param + " V2")
        param = param.split("//").join("/")
        console.log(param + " V3")
    }
    console.log("Param: " + param)
    
    if (isApi && req.method === "GET") {
        var path = "/db/apps/doc/" + param
        var text = ""
        console.log("Path: " + path)
        connection.get(path, function(res) {
            // collect input and print it out upon end
            var data = [];
            res.on("data", function(chunk) {
                data.push(chunk);
            });
            res.on("end", function() {
                text += data.join("");
                console.log(data.join(""));
            }); 
            res.on("error", function(err) {
                console.log("error: " + err);
            });
        });
        console.log("text: " + text);
        res.write(text);
        res.end();
        //console.log("Text: " + text);
    }
    if (isApi && req.method === "POST") {
        path = param
        //path = "C:/Users/Aleksa/Documents/GitHub/XMLWebServices/Nistagram/Nistagram.Agents/Nistagram.Agents/Upload/files/c7613891927031133.xml"
        console.log("Path: " + path)
        connection.store(path, "/db/apps/doc/");
        //response headers
        res.writeHead(200, { "Content-Type": "application/json" });
        //set the response
        res.write("File written to ExistDB!");
        //end the response
        res.end();
    }
});

server.listen(PORT, () => {
    console.log(`server started on port: ${PORT}`);
});
 
//GET METODA ZA XML BAZU

//var filePath = args[0]