﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Data.Model
{
    [Table("tbMessages")]
    public class Message
    {
        [Key]
        public Guid Id { get; set; }
        public string UserSender1Id { get; set; }
        public string UserReciever2Id { get; set; }
        public string Text { get; set; }
        public string LocalUrl { get; set; }
        public string WebUrl { get; set; }
        public DateTime TimeOfSending { get; set; }
    }
}
