﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Data.Model
{
    [Table("tbMessageConntacts")]
    public class MessageConntact
    {
        public MessageConntact()
        {
        }

        public MessageConntact(string user1Id, string user2Id)
        {
            Id = Guid.NewGuid();
            User1Id = user1Id;
            User2Id = user2Id;
        }

        [Key]
        public Guid Id { get; set; }
        public string User1Id { get; set; }
        public string User2Id { get; set; }
    }
}
