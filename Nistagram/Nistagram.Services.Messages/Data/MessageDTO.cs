﻿using Nistagram.Services.Messages.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Data
{
    public class MessageDTO
    {
        public MessageDTO()
        {

        }

        public MessageDTO(Message messages)
        {
            this.Text = messages.Text;
            this.UserSender1Id = messages.UserSender1Id;
            this.UserReciever2Id = messages.UserReciever2Id;
        }

        public string UserSender1Id { get; set; }
        public string UserReciever2Id { get; set; }
        public string Text { get; set; }
    }
}
