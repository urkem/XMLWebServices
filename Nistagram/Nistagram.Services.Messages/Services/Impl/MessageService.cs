﻿using Microsoft.AspNetCore.Http;
using Nistagram.Services.Messages.Data;
using Nistagram.Services.Messages.Data.Model;
using Nistagram.Services.Messages.Messaging;
using Nistagram.Services.Messages.Messaging.Messages;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Services.Impl
{
    public class MessageService : IMessageService
    {
        public static ManualResetEvent oSignalEvent;
        public static bool isConnected;


        private readonly ApplicationDbContext _context;
        private readonly IReactNotifier _notificationNotifier;
        private readonly IPrivacyNotifier _privacyNotifier;

        public MessageService(ApplicationDbContext context, IReactNotifier notificationNotifier, IPrivacyNotifier privacyNotifier)
        {
            _context = context;
            _notificationNotifier = notificationNotifier;
            _privacyNotifier = privacyNotifier;

            oSignalEvent = new ManualResetEvent(false);
        }

        public List<string> GetAllFriendsThatChatted(string userId)
        {
            return _context.Messages.Where(x => x.UserSender1Id == userId).Select(o => o.UserReciever2Id).Distinct().ToList();
        }

        public List<MessageDTO> GetHistoryChat(string user1Id, string user2Id)
        {
            return _context.Messages.Where(x => (x.UserSender1Id == user1Id && x.UserReciever2Id == user2Id) || 
                                                (x.UserSender1Id == user2Id && x.UserReciever2Id == user1Id))
                                    .OrderBy(x => x.TimeOfSending)
                                    .Select(o => new MessageDTO(o))
                                    .Distinct()
                                    .ToList();
        }

        public async Task SendMessageAsync(string senderId, string reciverId, string text, IFormFile[] files)
        {
            /*
            ManualResetEvent oSignalEvent = new ManualResetEvent(false);

            void SecondThread()
            {
                //DoStuff
                oSignalEvent.Set();
            }

            void Main()
            {
                //DoStuff
                //Call second thread
                System.Threading.Thread oSecondThread = new System.Threading.Thread(SecondThread);
                oSecondThread.Start();

                oSignalEvent.WaitOne(); //This thread will block here until the reset event is sent.
                oSignalEvent.Reset();
                */
            if(_context.MessageConntacts.Where(x => (x.User1Id == senderId && x.User2Id == reciverId) || (x.User2Id == senderId && x.User1Id == reciverId)).FirstOrDefault() == null)
            {
                _privacyNotifier.PrivacySender(senderId, reciverId);

                oSignalEvent.WaitOne();
                oSignalEvent.Reset();

                if (!isConnected)
                {
                    return;
                }
                else
                {
                    _context.MessageConntacts.Add(new MessageConntact(senderId, reciverId));
                }

            }


            Message message = new Message
            {
                Id = new Guid(),
                UserSender1Id = senderId,
                UserReciever2Id = reciverId,
                Text = text
            };

            var localPaths = new List<string>();
            foreach (var file in files)
            {
                localPaths.Add(await UploadFileToLocal(file));
            }

            if (localPaths.Count == 1)
            {
                message.LocalUrl = localPaths[0];
            }
            else
            {
                message.LocalUrl = JsonSerializer.Serialize(localPaths);
            }

            _context.Messages.Add(message);
            _notificationNotifier.NotificationSender(new Nistagram.Services.Messages.Data.NotificationDTO(reciverId, senderId, "MessageType", "User " + senderId + " message to " + reciverId, DateTime.Now));

            _context.SaveChanges();
        }

        public void SetConnected(PrivacyMessage canSendAMessage)
        {
            isConnected = canSendAMessage.isConntactable;
            oSignalEvent.Set();
        }

        public async Task<string> UploadFileToLocal(IFormFile file)
        {
            string fileName;
            string path = "";
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = DateTime.Now.Ticks + extension; //Create a new Name for the file due to security reasons.

                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files");

                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }

                path = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files",
                    fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return path;
        }
    }
}
