﻿using Microsoft.AspNetCore.Http;
using Nistagram.Services.Messages.Data;
using Nistagram.Services.Messages.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Services
{
    public interface IMessageService
    {
        public Task SendMessageAsync(string senderId, string reciverId, string text, IFormFile[] files);
        public List<MessageDTO> GetHistoryChat(string user1Id, string user2Id);
        public List<string> GetAllFriendsThatChatted(string userId);
        void SetConnected(PrivacyMessage canSendAMessage);
        Task<string> UploadFileToLocal(IFormFile file);
    }
}
