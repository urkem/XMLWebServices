﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Services.Messages.Migrations
{
    public partial class UrkeSeldba : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TimeOfSending",
                table: "tbMessages",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TimeOfSending",
                table: "tbMessages");
        }
    }
}
