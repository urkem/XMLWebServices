﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Services.Messages.Migrations
{
    public partial class Mess7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LocalUrl",
                table: "tbMessages",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "WebUrl",
                table: "tbMessages",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LocalUrl",
                table: "tbMessages");

            migrationBuilder.DropColumn(
                name: "WebUrl",
                table: "tbMessages");
        }
    }
}
