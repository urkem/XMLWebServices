﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Services.Messages.Migrations
{
    public partial class Mess3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tbMessageConntacts",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    User1Id = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User2Id = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tbMessageConntacts", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tbMessageConntacts");
        }
    }
}
