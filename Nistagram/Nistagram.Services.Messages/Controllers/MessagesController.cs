﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Messages.Data;
using Nistagram.Services.Messages.Services;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly ILogger<MessagesController> _logger;
        private readonly ApplicationDbContext _context;
        private readonly IMessageService _messageService;

        public MessagesController(ILogger<MessagesController> logger, ApplicationDbContext context, IMessageService messageService)
        {
            _logger = logger;
            _messageService = messageService;
            _context = context;
        }

        [HttpPost]
        [Route("/[action]/")]
        public IActionResult SendMessageAsync([FromForm] string recieverId, [FromForm] string text, [FromForm] IFormFile[] files)
        {
            //var webPaths = await _fileService.UploadFileToCloudinary(files);


            var profileUserId = User.FindFirstValue("name");
            _messageService.SendMessageAsync(profileUserId, recieverId, text, files);
            return Ok(); //OkResult(Ok()); //Task.FromResult(Ok());
        }

        [HttpGet]
        [Route("/[action]/")]
        public List<string> GetAllFriendsThatChatted()
        {
            var profileUserId = User.FindFirstValue("name"); 
            return _messageService.GetAllFriendsThatChatted(profileUserId);
        }

        [HttpGet]
        [Route("[controller]/[action]")]
        public List<MessageDTO> GetHistoryChat(string recieverId, string text)
        {
            var profileUserId = User.FindFirstValue("name");
            return _messageService.GetHistoryChat(profileUserId, recieverId);
        }
        /*
        GetAllFriendsThatChatted
        
        GetHistoryChat
        
        SendMessage*/
    }
}
