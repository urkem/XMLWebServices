﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagram.Services.Messages.Data;
using Nistagram.Services.Messages.Messaging;
using Nistagram.Services.Messages.Messaging.Messages;

namespace Nistagram.Services.Messages.Messaging.Impl
{
    public class PrivacyNotifier : IPrivacyNotifier
    {
        private readonly IRabbitManager _rabbitManager;
        private ILogger _logger;

        public PrivacyNotifier(IRabbitManager rabbitManager,  ILoggerFactory loggerFactory)
        {
            _rabbitManager = rabbitManager;
            _logger = loggerFactory.CreateLogger<PrivacyNotifier>();
        }

        public void PrivacySender(string user1Id, string user2Id)
        {
            ConnectMessage connectMessage = new ConnectMessage(user1Id, user2Id);
            
            _logger.LogInformation($"Sending PrivacyFSenderMessage {connectMessage}");

            _rabbitManager.Publish(connectMessage, "privacyFSender", "privacyFSender.send");
        }
    }
}
