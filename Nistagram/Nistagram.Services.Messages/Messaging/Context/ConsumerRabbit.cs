﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Messaging.Context
{
    public abstract class ConsumerRabbit : BackgroundService
    {
        private readonly ILogger _logger;
        private readonly string _hostName;
        private readonly string _userName;
        private readonly string _password;
        private readonly int _port;
        private IConnection _connection;
        private IModel _channel;
        private string _queueName;

        public ConsumerRabbit(ILoggerFactory loggerFactory, string exchangeName, string routingKey)
        {
            _logger = loggerFactory.CreateLogger<ConsumerRabbit>();
            _hostName = "rabbitmq";
            _userName = "guest";
            _password = "guest";
            _port = 5672;
            InitRabbitMq(exchangeName, routingKey);
        }

        private void InitRabbitMq(string exchangeName, string routingKey)
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = _hostName,
                Port = _port
            };

            _queueName = $"NistagramRabbitMQ-{exchangeName}-{routingKey}";

            _connection = connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, true, false, null);
            _channel.QueueDeclare(_queueName);
            _channel.QueueBind(_queueName, exchangeName, routingKey, null);

            _connection.ConnectionShutdown += RabbitMQ_ConnectionShutdown;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumer = new EventingBasicConsumer(_channel);
            consumer.Received += (ch, ea) =>
            {
                // received message  
                var content = System.Text.Encoding.UTF8.GetString(ea.Body.ToArray());
                _logger.LogInformation(content);
                // handle the received message  
                HandleMessage(content);
                _channel.BasicAck(ea.DeliveryTag, false);
            };

            consumer.Shutdown += OnConsumerShutdown;
            consumer.Registered += OnConsumerRegistered;
            consumer.Unregistered += OnConsumerUnregistered;
            consumer.ConsumerCancelled += OnConsumerConsumerCancelled;

            _channel.BasicConsume(_queueName, false, consumer);
            return Task.CompletedTask;
        }

        protected abstract void HandleMessage(string content);

        private void OnConsumerConsumerCancelled(object sender, ConsumerEventArgs e) { }
        private void OnConsumerUnregistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerRegistered(object sender, ConsumerEventArgs e) { }
        private void OnConsumerShutdown(object sender, ShutdownEventArgs e) { }
        private void RabbitMQ_ConnectionShutdown(object sender, ShutdownEventArgs e) { }

        public override void Dispose()
        {
            _channel.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}
