﻿using Nistagram.Services.Messages.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Messaging
{
    public interface IReactNotifier
    {
        void NotificationSender(NotificationDTO testSenderNotification);
    }
}
