﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Messages.Messaging
{
    public interface IPrivacyNotifier
    {
        void PrivacySender(string user1Id, string user2Id);
    }
}
