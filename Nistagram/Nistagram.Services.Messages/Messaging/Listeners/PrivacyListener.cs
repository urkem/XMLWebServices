﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagram.Services.Messages.Messaging.Context;
using Nistagram.Services.Messages.Data.Model;
using Nistagram.Services.Messages.Messaging.Context;
using Nistagram.Services.Messages.Services;
using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nistagram.Services.Messages.Data;
using Nistagram.Services.Messages.Messaging.Messages;

namespace Nistagram.Services.Messages.Messaging.Listeners
{
    public class PrivacyListener : ConsumerRabbit
    {
        private ILogger _logger;

        private IMessageService _messageService;

        private IServiceScopeFactory _serviceScopeFactory;


        public PrivacyListener(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory) : base(loggerFactory, "privacySender", "privacySender.send")
        {
            _logger = loggerFactory.CreateLogger<PrivacyListener>();
            _serviceScopeFactory = serviceScopeFactory;
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                _messageService = (IMessageService)scope.ServiceProvider.GetService(typeof(IMessageService));
                //_context = (ApplicationDbContext)scope.ServiceProvider.GetService(typeof(ApplicationDbContext));
                
            }
            //_context = context; 
        }

        protected override void HandleMessage(string content)
        {
            _logger.LogInformation($"Message received for UserCreatedMessage {content}");
            var canSendAMessage = JsonConvert.DeserializeObject<PrivacyMessage>(content);

            _messageService.SetConnected(canSendAMessage);
            //_testSenderService.Save(notification);
            //_testSenderService.DoSomething(createdUser.Id, createdUser.message);
        }
    }
}
