﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Neo4jClient;
using Nistagra.Services.Followers.Domain;
using Nistagra.Services.Followers.Domain.DTO;
using Nistagra.Services.Followers.Domain.Models;
using Nistagra.Services.Followers.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FollowersController : ControllerBase
    {
        private readonly ILogger<FollowersController> _logger;
        private readonly IGraphClient _graphClient;
        private readonly INeoService _neoService;

        public FollowersController(ILogger<FollowersController> logger,
            IGraphClient graphClient, INeoService neoService)
        {
            _logger = logger;
            _graphClient = graphClient;
            _neoService = neoService;

            _neoService.SetUniqueConstraint(_graphClient);
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> AcceptRequest(string followerId, string followingId)
        {
            if (await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_blockStr, _graphClient, true)
                || await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_followStr, _graphClient)
                )
            {
                return Problem("Accept not possible.");
            }

            if(!await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_requestStr, _graphClient))
            {
                return Problem("Request does not exist.");
            }

            await _neoService.AcceptRequest(followerId, followingId, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> CreateFollow(string followerId, string followingId)
        {

            if (await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_blockStr, _graphClient, true)
                || await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_followStr, _graphClient)
                || await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_requestStr, _graphClient)
                )
            {
                return Problem("Follow not possible.");
            }

            await _neoService.CreateUniqueRelationship(followerId, followingId, Constants.c_followStr, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> CreateContact(string senderId, string receiverId)
        {
            if (await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_blockStr, _graphClient, true)
                || await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_followStr, _graphClient)
                || await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_requestStr, _graphClient)
                || await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_contactStr, _graphClient)
                )
            {
                return Problem("Follow not possible.");
            }

            await _neoService.CreateUniqueRelationship(senderId, receiverId, Constants.c_contactStr, _graphClient);
            await _neoService.CreateUniqueRelationship(receiverId, senderId, Constants.c_contactStr, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> DeleteFollow(string followerId, string followingId)
        {
            if (!await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_followStr, _graphClient))
            {
                return Problem("Follow doesn't exist.");
            }

            await _neoService.DeleteFollow(followerId, followingId, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> DeleteRequest(string followerId, string followingId)
        {
            if (!await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_requestStr, _graphClient))
            {
                return Problem("Request doesn't exist.");
            }

            await _neoService.DeleteRequest(followerId, followingId, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> DeleteBlock(string followerId, string followingId)
        {
            if (!await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_blockStr, _graphClient))
            {
                return Problem("Block doesn't exist.");
            }

            await _neoService.DeleteBlock(followerId, followingId, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> CreateFollowRequest(string followerId, string followingId)
        {
            if (await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_blockStr, _graphClient, true) 
                || await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_followStr, _graphClient)
                || await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_requestStr, _graphClient)
                )
            {
                return Problem("Request not possible.");
            }

            await _neoService.CreateUniqueRelationship(followerId, followingId, Constants.c_requestStr, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{followerId}/{followingId}")]
        public async Task<IActionResult> Block(string followerId, string followingId)
        {
            if(await _neoService.CheckIfRelationshipExists(followerId, followingId, Constants.c_blockStr, _graphClient, true))
            {
                return Problem("Block not possible.");
            }

            await _neoService.Block(followerId, followingId, _graphClient);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/{guid}")]
        public async Task<ActionResult> CreateNew(string guid)
        {
            if (await _neoService.CheckIfUserExists(guid, _graphClient))
            {
                return Problem("ID already exists.");
            }

            var result = await _neoService.CreateUser(guid, _graphClient);

            return Ok(result);
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public async Task<IEnumerable<string>> GetFollowers(string userId)
        {
            if (!await _neoService.CheckIfUserExists(userId, _graphClient))
            {
                return null;
            }

            var result = await _neoService.GetFollowers(userId, _graphClient);

            return result.Select(x => x.Id).ToList();
        }

        [HttpGet]
        [Route("[controller]/[action]/{senderId}/{receiverId}")]
        public async Task<bool> CanSendMesssage(string senderId, string receiverId)
        {
            if (!await _neoService.CheckIfUserExists(senderId, _graphClient) ||
                !await _neoService.CheckIfUserExists(receiverId, _graphClient))
            {
                return false;
            }

            if (await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_blockStr, _graphClient, true)
                || await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_followStr, _graphClient, true)
                || (!(await _neoService.CheckIfRelationshipExists(senderId, receiverId, Constants.c_contactStr, _graphClient, true)))
                )
            {
                return false;
            }

            return true;
        }

        [HttpGet]
        [Route("[controller]/[action]/{searchPattern}")]
        public async Task<List<string>> SearchById(string searchPattern)
        {
            var result = (await _neoService.SearchUsers(searchPattern, _graphClient)).ToList();
            return result.ToList();
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public async Task<IEnumerable<string>> GetFollowings(string userId)
        {
            if (!await _neoService.CheckIfUserExists(userId, _graphClient))
            {
                return null;
            }

            var result = await _neoService.GetFollowings(userId, _graphClient);

            return result.Select(x => x.Id).ToList();
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public async Task<IEnumerable<RecommendationDTO>> GetRecommendations(string userId)
        {
            if (!await _neoService.CheckIfUserExists(userId, _graphClient))
            {
                return null;
            }

            var result = await _neoService.FindRecommendationsForUser(userId, _graphClient);

            foreach(var user in result)
            {
                user.MutualFriendId = (await _neoService
                    .FindFriendFollowers(userId, user.RecommendedId, _graphClient)).FirstOrDefault();
            }

            return result;
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public async Task<IEnumerable<string>> GetFriendFollowers(string userId, string targetId)
        {
            if (!await _neoService.CheckIfUserExists(userId, _graphClient) 
                || !await _neoService.CheckIfUserExists(targetId, _graphClient))
            {
                return null;
            }

            var result = await _neoService.FindFriendFollowers(userId, targetId, _graphClient);

            return result;
        }
    }
}
