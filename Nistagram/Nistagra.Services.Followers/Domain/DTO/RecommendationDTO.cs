﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Domain.DTO
{
    public class RecommendationDTO
    {
        public string RecommendedId { get; set; }
        public int Strength { get; set; }
        public string MutualFriendId { get; set; }
    }
}
