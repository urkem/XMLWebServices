﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Domain
{
    public static class Constants
    {
        public static string c_followStr = "FOLLOW";
        public static string c_requestStr = "REQUEST";
        public static string c_blockStr = "BLOCK";
        public static string c_contactStr = "CONTACT";
    }
}
