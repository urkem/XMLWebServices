﻿using Neo4jClient;
using Nistagra.Services.Followers.Data;
using Nistagra.Services.Followers.Domain;
using Nistagra.Services.Followers.Domain.DTO;
using Nistagra.Services.Followers.Domain.Models;
using Nistagra.Services.Followers.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Service.Impl
{
    public class NeoService : INeoService
    {
        private readonly IReactNotifier _notificationNotifier;
        private readonly IGraphClient _graphClient;

        public NeoService(IGraphClient graphClient, IReactNotifier notificationNotifier)
        {
            _notificationNotifier = notificationNotifier;
            _graphClient = graphClient;
        }

        public async Task CreateUniqueRelationship(string person1Id, string person2Id, string relationshipName, IGraphClient client)
        {
            _notificationNotifier.NotificationSender(new NotificationDTO(person2Id, person1Id, "FollowerType", "Create Unique Relationship " + person2Id + " follows to " + person1Id, DateTime.Now));

            await client.Cypher
                .Match("(f:User), (t:User)")
                .Where((NodeUser f) => f.Id == person1Id)
                .AndWhere((NodeUser t) => t.Id == person2Id)
                .Create("(f)-[:" + relationshipName.ToUpper() + "]->(t)").ExecuteWithoutResultsAsync();
        }

        public async Task Block(string userId, string blockedId, IGraphClient client)
        {
            await DeleteFollow(userId, blockedId, client);
            await DeleteFollow(blockedId, userId, client);
            await DeleteContact(blockedId, userId, client);
            await DeleteContact(userId, blockedId, client);
            await DeleteRequest(userId, blockedId, client);
            await DeleteRequest(blockedId, userId, client);

            await CreateUniqueRelationship(userId, blockedId, Constants.c_blockStr, client);
        }

        public async Task<NodeUser> CreateUser(string userId, IGraphClient client)
        {
            NodeUser user = new NodeUser();
            user.Id = userId;

            client.Cypher
                .Create("(user:User $usr)")
                .WithParam("usr", user)
                .ExecuteWithoutResultsAsync()
                .Wait();

            return user;
        }

        public async Task<IEnumerable<NodeUser>> GetFollowers(string userId, IGraphClient client)
        {
            return await GetIngoingRelationship(userId, Constants.c_followStr, client);
        }

        public async Task<IEnumerable<NodeUser>> GetFollowings(string userId, IGraphClient client)
        {
            return await GetOutgoingRelationships(userId, Constants.c_followStr, client);
        }

        public async Task<IEnumerable<NodeUser>> GetRequests(string userId, IGraphClient client)
        {
            return await GetIngoingRelationship(userId, Constants.c_requestStr, client);
        }

        public async Task<IEnumerable<NodeUser>> GetOutgoingRelationships(string userId, string relationshipName, IGraphClient client)
        {
            return await client.Cypher
              .Match("(u:User { Id: '" + userId.ToString() + "' })-[" + relationshipName.ToUpper() + "]->(b)")
              .Return<NodeUser>("b")
              .ResultsAsync;
        }

        public async Task DeleteRelationship(string requesterId, string targetId, string relationshipName, IGraphClient clinet)
        {
            var r = await clinet.Cypher
                .Match("(u:User { Id: '" + requesterId.ToString() + "' })-[r:" + relationshipName.ToUpper() + "]->(l:User {Id: '" + targetId.ToString() + "'})")
                .Return<object>("r")
                .ResultsAsync;

            var query = clinet.Cypher
                .Match("(u:User { Id: '" + requesterId.ToString() + "' })-[r:" + relationshipName.ToUpper() + "]->(l:User {Id: '" + targetId.ToString() + "'})")
                .Return<object>("r");

            await clinet.Cypher
                .Match("(u:User { Id: '" + requesterId.ToString() + "' })-[r:" + relationshipName.ToUpper() + "]->(l:User {Id: '" + targetId.ToString() + "'})")
                .Delete("r")
                .ExecuteWithoutResultsAsync();
        }

        public async Task DeleteBlock(string userId, string requesterId, IGraphClient client)
            => await DeleteRelationship(userId, requesterId, Constants.c_blockStr, client);

        public async Task DeleteRequest(string userId, string requesterId, IGraphClient client)
            => await DeleteRelationship(userId, requesterId, Constants.c_requestStr, client);

        public async Task DeleteFollow(string userId, string requesterId, IGraphClient client)
            => await DeleteRelationship(userId, requesterId, Constants.c_followStr, client);

        public async Task DeleteContact(string userId, string requesterId, IGraphClient client)
            => await DeleteRelationship(userId, requesterId, Constants.c_contactStr, client);

        public async Task AcceptRequest(string userId, string requesterId, IGraphClient client)
        {
            await DeleteRequest(userId, requesterId, client);
            await CreateUniqueRelationship(requesterId, userId, Constants.c_followStr, client);
        }

        private async Task<IEnumerable<NodeUser>> GetIngoingRelationship(string userId, string relationshipName, IGraphClient client)
        {
            return await client.Cypher
              .Match("(u:User { Id: '" + userId.ToString() + "' })<-[" + relationshipName.ToUpper() + "]-(b)")
              .Return<NodeUser>("b")
              .ResultsAsync;
        }

        public async Task SetUniqueConstraint(IGraphClient client)
        {
            await client.Cypher.Create("CONSTRAINT ON (c:User) ASSERT c.Id IS UNIQUE").ExecuteWithoutResultsAsync();
        }

        public async Task<bool> CheckIfUserExists(string userId, IGraphClient graphClient)
        {
            var r = await graphClient.Cypher
                .Match("(u:User { Id: '" + userId.ToString() + "' })")
                .Return<object>("u")
                .ResultsAsync;

            return r.Count() != 0;
        }

        public async Task<bool> CheckRelationshipExists(string userId, string requesterId, string relationship, IGraphClient graphClient)
        {
            var r = await graphClient.Cypher
                .Match("(u:User { Id: '" + userId.ToString() + "' })-[r:" + relationship.ToUpper() + "]->(l:User {Id: '" + requesterId.ToString() + "'})")
                .Return<object>("r")
                .ResultsAsync;

            return r.Count() != 0;
        }

        public async Task<List<RecommendationDTO>> FindRecommendationsForUser(string userId, IGraphClient graphClient)
        {
            var result = await graphClient.Cypher
                .Match("(u:User {Id:'" + userId + "'})-[:FOLLOW]->(m)-[:FOLLOW]->(recUser)")
                .Where("NOT(u) -[] - (recUser) ")
                .With("recUser.Id AS Recommended, count(*) AS Strength")
                .Return((Recommended, Strength) => new RecommendationDTO
                {
                    RecommendedId = Recommended.As<String>(),
                    Strength = Strength.As<Int32>()
                })
                .ResultsAsync;

            return result.OrderByDescending(o => o.Strength).ToList();
        }

        public async Task<bool> CheckIfRelationshipExists(string userId, string requesterId, string relationship, IGraphClient graphClient, bool isBothWays)
        {
            var isValid = await CheckRelationshipExists(userId, requesterId, relationship, graphClient);

            if(isBothWays)
            {
                isValid = isValid || await CheckRelationshipExists(requesterId, userId, relationship, graphClient); 
            }

            return isValid;
        }

        public async Task<List<string>> FindFriendFollowers(string userId, string targetId, IGraphClient graphClient)
        {
            var result = await graphClient.Cypher
                .Match("(u: User { Id: '" + userId + "'})-[:FOLLOW]->(m)-[:FOLLOW]->(rec: User { Id: '" + targetId + "'})")
                .Return<NodeUser>("m")
                .ResultsAsync;

            return result.Select(x => x.Id).ToList();
        }

        public async Task<IEnumerable<string>> SearchUsers(string searchPattern, IGraphClient graphClient)
        {
            var result = await graphClient.Cypher
                .Match("(u: User)")
                .Where("u.Id CONTAINS '" + searchPattern + "'")
                .Return<NodeUser>("u")
                .ResultsAsync;

            return result.Select(x => x.Id);
        }

        public async Task<bool> CanSendMesssage(string senderId, string receiverId)
        {
            if (!await CheckIfUserExists(senderId, _graphClient) ||
                !await CheckIfUserExists(receiverId, _graphClient))
            {
                return false;
            }

            if (await CheckIfRelationshipExists(senderId, receiverId, Constants.c_blockStr, _graphClient, true)
                || await CheckIfRelationshipExists(senderId, receiverId, Constants.c_followStr, _graphClient, true)
                || (!(await CheckIfRelationshipExists(senderId, receiverId, Constants.c_contactStr, _graphClient, true)))
                )
            {
                return false;
            }

            return true;
        }
    }
}
