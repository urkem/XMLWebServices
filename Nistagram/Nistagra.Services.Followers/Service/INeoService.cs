﻿using Neo4jClient;
using Nistagra.Services.Followers.Domain.DTO;
using Nistagra.Services.Followers.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Service
{
    public interface INeoService
    {
        public Task CreateUniqueRelationship(string person1Id, string person2Id, string relationshipName, IGraphClient client);
        public Task Block(string userId, string blockedId, IGraphClient client);
        public Task<NodeUser> CreateUser(string userId, IGraphClient client);
        public Task<IEnumerable<NodeUser>> GetFollowers(string userId, IGraphClient client);
        public Task<IEnumerable<NodeUser>> GetRequests(string userId, IGraphClient client);
        public Task AcceptRequest(string userId, string requesterId, IGraphClient client);
        public Task<IEnumerable<NodeUser>> GetFollowings(string userId, IGraphClient client);
        public Task DeleteRequest(string userId, string requesterId, IGraphClient client);
        public Task DeleteFollow(string userId, string requesterId, IGraphClient client);
        public Task DeleteBlock(string userId, string requesterId, IGraphClient client);

        public Task SetUniqueConstraint(IGraphClient client);
        public Task<bool> CheckIfRelationshipExists(string userId, string requesterId, string relationship, IGraphClient graphClient, bool isBothWays = false);
        public Task<bool> CheckIfUserExists(string guid, IGraphClient graphClient);
        public Task<List<RecommendationDTO>> FindRecommendationsForUser(string userId, IGraphClient graphClient);
        public Task<List<string>> FindFriendFollowers(string userId, string targetId, IGraphClient graphClient);
        public Task<IEnumerable<string>> SearchUsers(string searchPattern, IGraphClient graphClient);
        public Task<bool> CanSendMesssage(string user1Id, string user2Id);
    }
}
