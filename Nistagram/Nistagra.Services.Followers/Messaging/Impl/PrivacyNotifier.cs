﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagra.Services.Followers.Data;
using Nistagra.Services.Followers.Messaging;
using Nistagra.Services.Followers.Messaging.Messages;

namespace Nistagra.Services.Followers.Messaging.Impl
{
    public class PrivacyNotifier : IPrivacyNotifier
    {
        private readonly IRabbitManager _rabbitManager;
        private ILogger _logger;

        public PrivacyNotifier(IRabbitManager rabbitManager,  ILoggerFactory loggerFactory)
        {
            _rabbitManager = rabbitManager;
            _logger = loggerFactory.CreateLogger<PrivacyNotifier>();
        }

        public void PrivacySender(PrivacyMessage privacyMessage)
        {
            _logger.LogInformation($"Sending PrivacySenderMessage {privacyMessage}");

            _rabbitManager.Publish(privacyMessage, "privacySender", "privacySender.send");
        }
    }
}
