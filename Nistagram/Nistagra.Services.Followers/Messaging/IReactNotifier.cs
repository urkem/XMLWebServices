﻿using Nistagra.Services.Followers.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Messaging
{
    public interface IReactNotifier
    {
        void NotificationSender(NotificationDTO testSenderNotification);
    }
}
