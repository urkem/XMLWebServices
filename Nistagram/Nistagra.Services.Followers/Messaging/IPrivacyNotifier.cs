﻿using Nistagra.Services.Followers.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Messaging
{
    public interface IPrivacyNotifier
    {
        void PrivacySender(PrivacyMessage privacyMessage);
    }
}
