﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagra.Services.Followers.Messaging.Context;
using Nistagra.Services.Followers.Service;
using Nistagra.Services.Followers.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nistagra.Services.Followers.Messaging;
using Nistagra.Services.Followers.Messaging.Messages;

namespace Nistagra.Services.Followers.Messaging.Listeners
{
    public class PrivacyListener : ConsumerRabbit
    {
        private ILogger _logger;

        private INeoService _neoService;

        private IServiceScopeFactory _serviceScopeFactory;

        private IPrivacyNotifier _privacyNotifier;


        public PrivacyListener(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory) : base(loggerFactory, "privacyFSender", "privacyFSender.send")
        {
            _logger = loggerFactory.CreateLogger<PrivacyListener>();
            _serviceScopeFactory = serviceScopeFactory;
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                _neoService = (INeoService)scope.ServiceProvider.GetService(typeof(INeoService));
                _privacyNotifier = (IPrivacyNotifier)scope.ServiceProvider.GetService(typeof(IPrivacyNotifier));
                //_context = (ApplicationDbContext)scope.ServiceProvider.GetService(typeof(ApplicationDbContext));

            }
            //_context = context; 
        }

        protected override async void HandleMessageAsync(string content)
        {
            _logger.LogInformation($"Message received for UserCreatedMessage {content}");
            var canSendAMessage = JsonConvert.DeserializeObject<ConnectMessage>(content);

            ;
            PrivacyMessage p = new PrivacyMessage(await _neoService.CanSendMesssage(canSendAMessage.user1Id, canSendAMessage.user2Id));

            _privacyNotifier.PrivacySender(p);

            //_testSenderService.Save(notification);
            //_testSenderService.DoSomething(createdUser.Id, createdUser.message);
        }
    }
}
