﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Messaging.Messages
{
    public class PrivacyMessage
    {
        public PrivacyMessage(bool isConntactable)
        {
            this.isConntactable = isConntactable;
        }
        public bool isConntactable { get; set; }
    }
}
