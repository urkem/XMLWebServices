﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers.Messaging.Messages
{
    public class ConnectMessage
    {
        public ConnectMessage(string user1Id, string user2Id)
        {
            this.user1Id = user1Id;
            this.user2Id = user2Id;
        }
        public string user1Id { get; set; }
        public string user2Id { get; set; }
    }
}
