using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.ObjectPool;
using Microsoft.OpenApi.Models;
using Neo4jClient;
using Nistagra.Services.Followers.Messaging;
using Nistagra.Services.Followers.Messaging.Context;
using Nistagra.Services.Followers.Messaging.Impl;
using Nistagra.Services.Followers.Messaging.Listeners;
using Nistagra.Services.Followers.Service;
using Nistagra.Services.Followers.Service.Impl;
using Prometheus;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagra.Services.Followers
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //var neo4jClient = new GraphClient(new Uri("http://neo4j:7474"), "neo4j", "neo4j");
            var neo4jClient = new GraphClient(new Uri("http://neo4j:7474"));
            var task = neo4jClient.ConnectAsync();
            task.Wait();

            services.AddSingleton<IPooledObjectPolicy<IModel>, RabbitModelPooledObjectPolicy>();
            services.AddSingleton<IRabbitManager, RabbitManager>();
            //services.AddSingleton<IReactionService, ReactService>();
            services.AddSingleton<IReactNotifier, ReactNotifier>();
            services.AddSingleton<IPrivacyNotifier, PrivacyNotifier>();

            services.AddScoped<INeoService, NeoService>();

            services.AddHostedService<PrivacyListener>();

            services.AddSingleton<IGraphClient>(neo4jClient);


            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder
                    .WithOrigins(new[] { "http://localhost:3000" })
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Nistagram.auth", Version = "v1" });
            });
            services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Nistagram.Services.Followers v1"));
            }

            //app.UseHttpsRedirection();

            app.UseRouting();
            app.UseMetricServer(); // from using Prometheus
            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
