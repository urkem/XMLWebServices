﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Admin.Data.Model
{
    [Table("tbFeedbacks")]
    public class Feedback
    {
        [Key]
        public Guid Id { get; set; }
        public string? UserId { get; set; } //If user wants to stay anonymous
        public string Text { get; set; }
    }
}
