﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Admin.Data.Model
{
    [Table("tbVerificationRequests")]
    public class VerificationRequest
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string ImageUrl { get; set; }
        public bool IsVerified { get; set; }
        public DateTime CreationTime { get; private set; }

        public VerificationRequest()
        {
            CreationTime = DateTime.Now;
        }
    }
}
