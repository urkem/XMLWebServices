﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Services.Admin.Migrations
{
    public partial class TestAdminDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_VerificationRequests",
                table: "VerificationRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Feedbacks",
                table: "Feedbacks");

            migrationBuilder.RenameTable(
                name: "VerificationRequests",
                newName: "tbVerificationRequests");

            migrationBuilder.RenameTable(
                name: "Feedbacks",
                newName: "tbFeedbacks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tbVerificationRequests",
                table: "tbVerificationRequests",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_tbFeedbacks",
                table: "tbFeedbacks",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_tbVerificationRequests",
                table: "tbVerificationRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_tbFeedbacks",
                table: "tbFeedbacks");

            migrationBuilder.RenameTable(
                name: "tbVerificationRequests",
                newName: "VerificationRequests");

            migrationBuilder.RenameTable(
                name: "tbFeedbacks",
                newName: "Feedbacks");

            migrationBuilder.AddPrimaryKey(
                name: "PK_VerificationRequests",
                table: "VerificationRequests",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Feedbacks",
                table: "Feedbacks",
                column: "Id");
        }
    }
}
