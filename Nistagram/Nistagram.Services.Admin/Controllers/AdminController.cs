﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Admin.Data;
using Nistagram.Services.Admin.Data.Model;
using Nistagram.Services.Admin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Admin.Controllers
{
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly ILogger<AdminController> _logger;
        private readonly IFileService _fileService;
        private readonly ApplicationDbContext _context;

        public AdminController(ILogger<AdminController> logger, ApplicationDbContext context, IFileService fileService)
        {
            _logger = logger;
            _context = context;
            _fileService = fileService;
        }

        [HttpPost]
        [Route("[controller]/[action]/")]
        public async Task<IActionResult> Upload([FromForm] string userId, [FromForm] IFormFile[] files)
        {
            var paths = await _fileService.UploadFileToCloudinary(files);

            VerificationRequest request = new VerificationRequest();
            request.ImageUrl = paths[0];
            request.UserId = userId;
            request.IsVerified = false;

            _context.VerificationRequests.Add(request);

            await _context.SaveChangesAsync();

            var c = _context.VerificationRequests.Where(x => x.UserId == "zdjela").FirstOrDefault();

            return Ok();
        }

        [HttpGet]
        [Route("[controller]/[action]")]
        public List<VerificationRequest> GetAllRequests()
        {
            var allRequests = _context.VerificationRequests.ToList();

            return allRequests;
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public VerificationRequest GetByUserId(string userId)
        {
            return _context.VerificationRequests.Where(x => x.UserId == userId).FirstOrDefault();
        }

        [HttpPost]
        [Route("[controller]/[action]/{userId}")]
        public IActionResult VerifyUserById(string userId)
        {
            var request = _context.VerificationRequests.Where(x => x.UserId == userId).FirstOrDefault();

            if(request != null)
            {
                request.IsVerified = true;

                _context.SaveChanges();
                //return Ok();
            }

            return Ok();
        }

    }
}
