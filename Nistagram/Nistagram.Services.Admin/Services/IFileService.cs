﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Admin.Services
{
    public interface IFileService
    {
        Task<List<string>> UploadFileToCloudinary(IFormFile[] file);
    }
}
