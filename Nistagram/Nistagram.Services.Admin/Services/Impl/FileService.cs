﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Admin.Services.Impl
{
    public class FileService : IFileService
    {
        private readonly Cloudinary _cloudinary;

        public FileService(Cloudinary cloudinary)
        {
            _cloudinary = cloudinary;
        }

        public async Task<List<string>> UploadFileToCloudinary(IFormFile[] files)
        {
            var urls = new List<string>();
            foreach (var file in files)
            {
                var result = await _cloudinary.UploadAsync(new ImageUploadParams
                {
                    File = new FileDescription(file.FileName,
                            file.OpenReadStream()),
                    Tags = "nistagram_admin_service"
                }).ConfigureAwait(false);

                urls.Add(result.Url.ToString());
            }

            return urls;
        }
    }
}
