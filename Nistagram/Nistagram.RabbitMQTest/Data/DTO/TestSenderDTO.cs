﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Data.DTO
{
    public class TestSenderDTO
    {
        public TestSenderDTO()
        { 
        }
            
        public TestSenderDTO(Guid id, string messageSend)
        {
            Id = id;
            message = messageSend;
        }

        public Guid Id { get; }
        public string message { get; }
    }
}
