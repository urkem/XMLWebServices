﻿using Microsoft.AspNetCore.SignalR;
using Nistagram.RabbitMQTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Services.Impl
{
    public class ChatHub :  Hub<IChatClient>
    {
        public async Task SendMessage(ChatMessage message)
        {
            await Clients.All.ReceiveMessage(message);
        }
    }
}
