﻿
using Nistagram.RabbitMQTest.Data.DTO;
using Nistagram.RabbitMQTest.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Services.Impl
{
    public class TestSenderService : ITestSenderService
    {
        private readonly ITestSenderNotifier _testSenderNotifier;
        public TestSenderService( ITestSenderNotifier testSenderNotifier)
        {
            _testSenderNotifier = testSenderNotifier;
        }


        public void DoSomething(Guid id, string message)
        {
            _testSenderNotifier.SendTestSender(new TestSenderDTO(id, message));
        }
    }
}
