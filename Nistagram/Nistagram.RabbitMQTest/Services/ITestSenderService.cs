﻿using Nistagram.RabbitMQTest.Data.DTO;
using System;

namespace Nistagram.RabbitMQTest.Services
{
    public interface ITestSenderService
    {
        public void DoSomething(Guid id, string message);
    }
}