﻿using Nistagram.RabbitMQTest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Services
{
    public interface IChatClient
    {
        Task ReceiveMessage(ChatMessage message);
    }
}
