﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Entities
{
    public class ChatMessage
    {
        public string User { get; set; }

        public string Message { get; set; }
    }
}
