﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Entities
{

    [Table("tbTestSenders")]
    public class TestSender //: /IEntity
    {
        public TestSender()
        {

        }

        public TestSender(Guid Id, string Message)
        {
            this.Id = Id;
            this.Message = Message;
        }

        /*
        public TestSender(TestSenderDto testSenderDto)
        {
            this.Id = Guid.NewGuid();
            this.Message = testSenderDto.Message;
        }*/

        [Key]
        public Guid Id { get; set; }


        public string Message { get; set; }

    }
}
