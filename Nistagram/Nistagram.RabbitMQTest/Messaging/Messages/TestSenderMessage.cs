﻿using Nistagram.RabbitMQTest.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Messaging.Messages
{

    public class TestSenderMessage
    {
        public TestSenderMessage(TestSenderDTO testSenderNotification)
        {
            Id = testSenderNotification.Id;
            Message = testSenderNotification.message;
        }

        public Guid Id { get; set; }
        private string Message { get; set; }
    }
}
