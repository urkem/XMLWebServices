﻿using Nistagram.RabbitMQTest.Data.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Messaging
{
    public interface ITestSenderNotifier
    {
        void SendTestSender(TestSenderDTO testSenderNotification);
    }
}
