﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagram.RabbitMQTest.Data.DTO;
using Nistagram.RabbitMQTest.Messaging.Context;
using Nistagram.RabbitMQTest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.RabbitMQTest.Messaging.Listeners
{
    public class TestSenderListener : ConsumerRabbit
    {
        private ILogger _logger;

        private ITestSenderService _testSenderService;

        private IServiceScopeFactory _serviceScopeFactory;

        public TestSenderListener(ILoggerFactory loggerFactory, IServiceScopeFactory serviceScopeFactory) : base(loggerFactory, "testSender", "testSender.send")
        {
            _logger = loggerFactory.CreateLogger<TestSenderListener>();
            _serviceScopeFactory = serviceScopeFactory;
            using (var scope = _serviceScopeFactory.CreateScope())
            {
                _testSenderService = (ITestSenderService)scope.ServiceProvider.GetService(typeof(ITestSenderService));
            }
        }

        protected override void HandleMessage(string content)
        {
            _logger.LogInformation($"Message received for UserCreatedMessage {content}");
            var createdUser = JsonConvert.DeserializeObject<TestSenderDTO>(content);

            _testSenderService.DoSomething(createdUser.Id, createdUser.message);
        }
    }
}
