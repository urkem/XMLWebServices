﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Nistagram.RabbitMQTest.Data;
using Nistagram.RabbitMQTest.Entities;
using Nistagram.RabbitMQTest.Services;

namespace Nistagram.RabbitMQTest.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SenderController : Controller
    {

        private readonly ApplicationDbContext _context;

        private readonly ITestSenderService _testSenderService;

        public SenderController(ApplicationDbContext context, ITestSenderService testSenderService)
        {
            _context = context;
            _testSenderService = testSenderService;
        }

        [HttpGet]
        public void Testing()
        {
            _testSenderService.DoSomething(Guid.NewGuid(), "Test 123 ... ");

        }

        // GET: SenderController/Details/5
        /*
        public async Task<ActionResult> GetDateFromPost(Guid id)
        {
            await _publishEndPoint.Publish(new SendIdToGetPost(id));
            return View();
        }*/

    }
}
