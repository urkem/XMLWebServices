﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services
{
    public interface IReactionService
    {
        public Task ReactAsync(string ProfileUserId, Guid Id, bool IsLiked);
    }
}
