using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Nistagram.Services.Posts.Data.Model;

namespace Nistagram.Services.Posts.Services
{
    public interface IFileService
    {
        string GetFile(string type, Guid entityId);
        void InsertFile(string type, Guid entityId);
        Task<string> UploadFileToS3(IFormFile file);
        Task<string> UploadFileToLocal(IFormFile file);
        Task<List<string>> UploadFileToCloudinary(IFormFile[] file);
        string GetTagsToJson(string description);
        Task<string> ExtractTextFromImageAsync(string imageUrl);
    }
}