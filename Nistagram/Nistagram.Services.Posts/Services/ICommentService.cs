﻿using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services
{
    public interface ICommentService
    {
        void CommentAsync(string UserId, Guid Profileid, string Content);
        List<CommentsDTO> GetCommentsByPost(Guid profileid);
    }
}
