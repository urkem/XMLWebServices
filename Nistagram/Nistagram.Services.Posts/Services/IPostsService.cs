﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services
{
    public interface IPostsService
    {
        List<PostsDTO> GetPostsByUser(string userId);
        List<PostsDTO> GetAllPosts();
        Task<List<PostsDTO>> GetPostsByFollowingUsers(List<string> followedUsersId, int daysAgo);
        List<PostsReactionDTO> GetPostsWithReactionByUser(string userName);
    }
}
