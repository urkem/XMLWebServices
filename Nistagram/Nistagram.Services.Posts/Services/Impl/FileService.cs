using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Amazon.Internal;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Nistagram.Services.Posts.Data.Model;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class FileService: IFileService
    {
        private readonly Cloudinary _cloudinary;

        public FileService(Cloudinary cloudinary)
        {
            _cloudinary = cloudinary;
        }

        public string GetFile(Guid entityId)
        {
            throw new NotImplementedException();
        }

        public string GetFile(string type, Guid entityId)
        {
            throw new NotImplementedException();
        }

        public void InsertFile(string type, Guid entityId)
        {
            throw new NotImplementedException();
        }

        public string GetTagsToJson(string description)
        {
            var regex = new Regex(@"(?<=#)\w+");
            var matches = regex.Matches(description);
            var tags = new List<string>();
            foreach (var m in matches)
            {
                tags.Add(m.ToString().ToLower());
            }

            return String.Join(",", tags.Select(x => x.ToString()).ToArray());
        }

        public async Task<List<string>> UploadFileToCloudinary(IFormFile[] files)
        {
            var urls = new List<string>();
            foreach(var file in files)
            {
                var result = await _cloudinary.UploadAsync(new ImageUploadParams
                {
                    File = new FileDescription(file.FileName,
                            file.OpenReadStream()),
                    Tags = "nistagram_album"
                }).ConfigureAwait(false);

                urls.Add(result.Url.ToString());
            }

            return urls;
        }

        public async Task<string> UploadFileToLocal(IFormFile file)
        {
            string fileName;
            string path = "";
            try
            {
                var extension = "." + file.FileName.Split('.')[file.FileName.Split('.').Length - 1];
                fileName = DateTime.Now.Ticks + extension; //Create a new Name for the file due to security reasons.

                var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files");

                if (!Directory.Exists(pathBuilt))
                {
                    Directory.CreateDirectory(pathBuilt);
                }

                path = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files",
                    fileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return path;
        }  

        public async Task<string> UploadFileToS3(IFormFile file)
        {
            using (var client = new AmazonS3Client(new AmazonS3Config
            {
                ServiceURL = "http://s3:9090",
                //ServiceURL = "http://localstack-s3:9090",
                ForcePathStyle = true,
            }))
            {
                using (var newMemoryStream = new MemoryStream())
                {
                    file.CopyTo(newMemoryStream);
                    var key = $"{file.FileName}";
                    var uploadRequest = new TransferUtilityUploadRequest
                    {
                        InputStream = newMemoryStream,
                        Key = key,
                        BucketName = "media",
                        CannedACL = S3CannedACL.PublicRead
                    };

                    var fileTransferUtility = new TransferUtility(client);
                    await fileTransferUtility.UploadAsync(uploadRequest);

                    return key;
                }
            }
        }

        public async Task<string> ExtractTextFromImageAsync(string imageUrl)
        {
            var encodedUrl = System.Net.WebUtility.UrlEncode(imageUrl);
            const string requestUri = "https://ocrly-image-to-text.p.rapidapi.com/?filename=sample.jpg&imageurl=";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = System.Net.Http.HttpMethod.Get,
                RequestUri = new Uri(requestUri + encodedUrl),
                Headers =
                {
                    { "x-rapidapi-key", "13da60266amshdd951ef043642d8p1e094djsn6ab61e4b29ee" },
        { "x-rapidapi-host", "ocrly-image-to-text.p.rapidapi.com" },
                },
            };
            using (var response = await client.SendAsync(request))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                return body;
            }
        }
    }
}