﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class CommentService : ICommentService
    {

        private readonly ApplicationDbContext _context;

        public CommentService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void CommentAsync(string UserId, Guid Profileid, string ContentText)
        {
            Comment comment = new Comment
            {
                Id = new Guid(),
                Content = ContentText,
                PostId = Profileid,
                UserId = UserId,
                TimeOfCommenting = DateTime.Now
            };

            _context.Comments.Add(comment);
            _context.SaveChangesAsync();
        }

        public List<CommentsDTO> GetCommentsByPost(Guid profileid)
        {
            return _context.Comments.Where(x => x.PostId == profileid).Select(x => new CommentsDTO(x)).ToList();
        }
    }
}
