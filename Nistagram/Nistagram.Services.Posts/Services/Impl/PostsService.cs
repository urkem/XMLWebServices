﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class PostsService : IPostsService
    {
        private readonly ApplicationDbContext _context;

        public PostsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<PostsDTO> GetPostsByUser(string userId)
        {
            return _context.Posts.Where(x => x.UserId == userId).Select(x => new PostsDTO(x)).ToList();
        }

        public List<PostsDTO> GetAllPosts()
        {
            return _context.Posts.Select(x => new PostsDTO(x)).ToList();//.OrderBy(o => o.TimeOfCreation).ToList();
        }

        public async Task<List<PostsDTO>> GetPostsByFollowingUsers(List<string> followedUsersId, int daysAgo)
        {
            return _context.Posts.Where(x => followedUsersId.Contains(x.UserId) 
                    && x.TimeOfCreation > DateTime.Now.AddDays(-daysAgo)).Select(x => new PostsDTO(x)).ToList();
        }

        public List<PostsReactionDTO> GetPostsWithReactionByUser(string userName)
        {
            var postIds = _context.Reactions.Where(x => x.UserId == userName).Select(x => x.PostId).ToList();
            var postsReactions = _context.Posts.Where(x => postIds.Contains(x.Id)).Select(x => new PostsReactionDTO(x)).ToList();

            return postsReactions;
        }
    }
}
