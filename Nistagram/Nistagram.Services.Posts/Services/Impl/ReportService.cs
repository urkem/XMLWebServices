﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class ReportService : IReportService
    {
        private readonly ApplicationDbContext _context;

        public ReportService(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Report(string UserId, Guid PostId, string Description)
        {
            //var r = _context.Reports.ToList();
            _context.Reports.Add(new Report()
            {
                Id = new Guid(),
                UserId = UserId,
                PostId = PostId,
                Content = Description
            });

            _context.SaveChangesAsync();

        }
    }
}
