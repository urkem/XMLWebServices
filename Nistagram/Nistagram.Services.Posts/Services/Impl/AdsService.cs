﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class AdsService : IAdsService
    {
        private readonly ApplicationDbContext _context;

        public AdsService(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Ad> GetForUser(int age, bool isShown)
        {
            if (isShown)
            {
                var ads2 = _context.Ads.ToList();
                var ads = _context.Ads
               .Where(x => x.Start.Date <= DateTime.Now.Date && x.End.Date >= DateTime.Now.Date
               && x.MinAge <= age && x.MaxAge >= age)
               .ToList();

                foreach (var ad in ads)
                {
                    ad.ViewsCount++;
                    _context.Update(ad);
                }
                _context.SaveChanges();
            }
           
            return _context.Ads
               .Where(x => x.Start.Date <= DateTime.Now.Date && x.End.Date >= DateTime.Now.Date
               && x.MinAge <= age && x.MaxAge >= age)
               .ToList();
        }
    }
}
