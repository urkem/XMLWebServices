﻿using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using Nistagram.Services.Posts.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services.Impl
{
    public class ReactService : IReactionService
    {
        private readonly ApplicationDbContext _context;
        private readonly IReactNotifier _notificationNotifier;

        public ReactService(ApplicationDbContext context, IReactNotifier notificationNotifier)
        {
            _context = context;
            _notificationNotifier = notificationNotifier;
        }

        public async Task ReactAsync(string ProfileUserId, Guid PostId, bool IsLiked)
        {
            Reaction reaction = _context.Reactions.Where(x => PostId == x.PostId && x.UserId == ProfileUserId).FirstOrDefault();
            if(reaction == null)
            {
                reaction = new Reaction
                {
                    Id = Guid.NewGuid(),
                    IsLiked = IsLiked,
                    UserId = ProfileUserId,
                    PostId = PostId
                };

                UpdateNumberOfLikes(PostId, IsLiked);
                _context.Reactions.Add(reaction);
            }
            else
            {
                if(reaction.IsLiked != IsLiked)
                {
                    ChangeNumberOfLikesAndDislikes(PostId, IsLiked);
                    reaction.IsLiked = IsLiked;
                    _context.Update(reaction);
                }
            }

            Post post = _context.Posts.Find(PostId);

            _notificationNotifier.NotificationSender(new Nistagram.Services.Posts.Data.NotificationDTO(ProfileUserId, post.ProfileUserId, "LikeDIslike", "User " + ProfileUserId + " reacted on post "+ post.Id, DateTime.Now));
            await _context.SaveChangesAsync();

        }

        private void ChangeNumberOfLikesAndDislikes(Guid PostId, bool IsLiked)
        {
            Post post = _context.Posts.Find(PostId);
            if (IsLiked)
            {
                post.NumberOfLikes++;
                post.NumberOfDislikes--;
            }
            else
            {
                post.NumberOfLikes--;
                post.NumberOfDislikes++;
            }
            _context.Posts.Update(post);
            _context.SaveChanges();
        }

        private void UpdateNumberOfLikes(Guid PostId, bool IsLiked)
        {
            Post post = _context.Posts.Find(PostId);

            if (IsLiked)
            {
                post.NumberOfLikes++;
            }
            else
            {
                post.NumberOfDislikes++;
            }

            _context.Posts.Update(post);
            _context.SaveChanges();
        }
    }
}
