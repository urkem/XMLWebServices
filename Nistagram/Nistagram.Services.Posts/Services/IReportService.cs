﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services
{
    public interface IReportService
    {
        public void Report(string UserId, Guid PostId, string Description);
    }
}
