﻿using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Services
{
    public interface IAdsService
    {
        List<Ad> GetForUser(int age, bool isShown = false);
    }
}
