﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Messaging;
using Nistagram.Services.Posts.Messaging.Messages;

namespace Nistagram.RabbitMQTest.Messaging.Impl
{
    public class ReactNotifier : IReactNotifier
    {
        private readonly IRabbitManager _rabbitManager;
        private ILogger _logger;

        public ReactNotifier(IRabbitManager rabbitManager,  ILoggerFactory loggerFactory)
        {
            _rabbitManager = rabbitManager;
            _logger = loggerFactory.CreateLogger<ReactNotifier>();
        }

        public void NotificationSender(NotificationDTO testSenderNotification)
        {
            testSenderNotification.ContentType = "ReactType";
            NotificationMessage json = new NotificationMessage(testSenderNotification); //notificationMessage = new NotificationMessage(testSenderNotification);
            //var json = JsonConvert.SerializeObject(notificationMessage);

            _logger.LogInformation($"Sending TestSenderMessage {json}");

            _rabbitManager.Publish(json, "testSender", "testSender.send");
        }
    }
}
