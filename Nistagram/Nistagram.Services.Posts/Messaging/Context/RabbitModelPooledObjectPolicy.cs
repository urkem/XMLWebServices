
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;

namespace Nistagram.Services.Posts.Messaging.Context

{
    public class RabbitModelPooledObjectPolicy: IPooledObjectPolicy<IModel>
    {
        private readonly IConnection _connection;
        private readonly string _hostName;
        private readonly string _userName;
        private readonly string _password;
        private readonly int _port;

        public RabbitModelPooledObjectPolicy()
        {
            _hostName = "rabbitmq";
            _userName = "guest";
            _password = "guest";
            _port = 5672;
            _connection = GetConnection();
        }

        private IConnection GetConnection()
        {
            var connectionFactory = new ConnectionFactory
            {
                HostName = _hostName, UserName = _userName, Password = _password, Port = _port
            };
            
            return connectionFactory.CreateConnection();
        }

        public IModel Create()
        {
            return _connection.CreateModel();
        }

        public bool Return(IModel obj)
        {
            if (obj.IsOpen)
            {
                return true;
            }
            else
            {
                obj?.Dispose();
                return false;
            }
        }
    }
}