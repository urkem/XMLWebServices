﻿using Nistagram.Services.Posts.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Messaging
{
    public interface IReactNotifier
    {
        void NotificationSender(NotificationDTO testSenderNotification);
    }
}
