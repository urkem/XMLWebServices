﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Services.Posts.Migrations
{
    public partial class postsPrivacyUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsPrivate",
                table: "tbPosts",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPrivate",
                table: "tbPosts");
        }
    }
}
