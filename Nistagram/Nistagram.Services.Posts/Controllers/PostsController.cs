﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using Nistagram.Services.Posts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Controllers
{
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly ILogger<PostsController> _logger;
        private readonly IFileService _fileService;
        private readonly IReactionService _reactionService;
        private readonly IReportService _reportService;
        private readonly ApplicationDbContext _context;
        private readonly IPostsService _postsService;
        private readonly ICommentService _commentService;
        private readonly IAdsService _adsService;

        public PostsController(ILogger<PostsController> logger, IFileService fileService, IPostsService postsService,
            IReactionService reactionService, ApplicationDbContext context, ICommentService commentService, 
            IReportService reportService, IAdsService adsService)
        {
            _logger = logger;
            _fileService = fileService;
            _postsService = postsService;
            _context = context;
            _reactionService = reactionService;
            _commentService = commentService;
            _reportService = reportService;
            _adsService = adsService;
        }
        
        [HttpPost]
        [Route("[controller]/[action]/")]
        public async Task<IActionResult> Upload([FromForm] string userId, [FromForm] string description, 
            [FromForm] IFormFile[] files, [FromForm] bool isPrivate = false )
        {
            var localPaths = new List<string>();
            foreach(var file in files)
            {
                localPaths.Add(await _fileService.UploadFileToLocal(file));
            }
            //var webPaths = await _fileService.UploadFileToCloudinary(files);

            Post post = new Post();
            post.Id = Guid.NewGuid();
            post.UserId = userId;
            post.IsPrivate = isPrivate;

            if(localPaths.Count == 1)
            {
                post.LocalUrl = localPaths[0];
                //post.WebUrl = webPaths[0];
            }
            else
            {
                post.LocalUrl = JsonSerializer.Serialize(localPaths);
                //post.WebUrl = JsonSerializer.Serialize(webPaths);
            }

            post.Description = description;
            post.NumberOfDislikes = 0;
            post.NumberOfLikes = 0;
            post.NumberOfComments = 0;
            post.TimeOfCreation = DateTime.Now;

            post.Tags = _fileService.GetTagsToJson(description);

            _context.Posts.Add(post);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        [Route("[controller]/[action]/{tag}")]
        public async Task<List<PostsDTO>> SearchPostsByTag(string tag)
        {
            return await _context.Posts
                .Where(x => !x.IsPrivate && x.Tags.Contains(tag.ToUpper()))
                .Select(x => new PostsDTO(x))
                .ToListAsync();
        }

        [HttpPost]
        [Route("[controller]/[action]/{userId}/{privacy}")]
        public async Task<IActionResult> ChangePostsPrivacy(string userId, bool privacy)
        {
            await _context.Posts.Where(x => x.UserId == userId).ForEachAsync(x => x.IsPrivate = privacy);

            return Ok();
        }

        [HttpPost]
        [Route("[controller]/[action]/")]
        public IActionResult React(Guid id, bool isLiked)
        {
            var profileUserId = User.FindFirstValue("name");
            _reactionService.ReactAsync(profileUserId, id, isLiked);
            return Ok();
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public List<PostsDTO> GetPostsByUser(string userId)
        { 
            return _postsService.GetPostsByUser(userId);
        }

        [HttpGet]
        [Route("[controller]/[action]")]
        public List<PostsDTO> GetAllPosts(int daysAgo = 30, int age = 20)
        {
            var adsForUser = _adsService.GetForUser(age, isShown: true);

            var posts = _postsService.GetAllPosts();

            var postsAndAds = adsForUser.Select(x => new PostsDTO(x)).ToList();

            posts.AddRange(postsAndAds);
            posts = posts.OrderBy(a => Guid.NewGuid()).ToList();

            return posts;
        }

        [HttpGet]
        [Route("[controller]/[action]")]
        public async Task<List<PostsDTO>> GetPostsByFollowedUsers(List<string> followedUsers, int daysAgo = 30, int age = 20)
        {
            var adsForUser = _adsService.GetForUser(age, isShown: true);

            var posts = await _postsService.GetPostsByFollowingUsers(followedUsers, daysAgo);

            var postsAndAds = adsForUser.Select(x => new PostsDTO(x)).ToList();

            posts.AddRange(postsAndAds);
            posts = posts.OrderBy(a => Guid.NewGuid()).ToList();

            return posts;
        }

        [Authorize]
        [HttpPost]
        [Route("[controller]/Comment/")]
        public IActionResult Comment(Guid PostId, string Content)
        {
            var userName = User.FindFirstValue("name");
            _commentService.CommentAsync(userName, PostId, Content);
            return Ok();
        }

        [HttpGet]
        [Route("[controller]/Comment/{PostId}")]
        public List<CommentsDTO> GetPostsComments(Guid PostId)
        {
            return _commentService.GetCommentsByPost(PostId);
        }

        [HttpGet]
        [Route("[controller]/[action]/")]
        public List<PostsReactionDTO> GetPostsWithReactionByUser()
        {
            var userName = User.FindFirstValue("name");
            return _postsService.GetPostsWithReactionByUser(userName);
        }

        [HttpGet]
        [Route("[controller]/[action]/{PostId}/{Description}")]
        public IActionResult Report(Guid PostId, string Description)
        {
            var userName = User.FindFirstValue("name");
            _reportService.Report(userName, PostId, Description);
            return Ok();
        }
    }
}
