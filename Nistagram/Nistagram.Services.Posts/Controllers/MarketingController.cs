﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using Nistagram.Services.Posts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Controllers
{
    public class MarketingController : Controller
    {
        private readonly ILogger<PostsController> _logger;
        private readonly IFileService _fileService;
        private readonly IAdsService _adsService;
        private readonly ApplicationDbContext _context;

        public MarketingController(ILogger<PostsController> logger, 
            IFileService fileService, IAdsService adsService, ApplicationDbContext context)
        {
            _logger = logger;
            _fileService = fileService;
            _context = context;
            _adsService = adsService;
        }

        private void ShowAd(int adId)
        {
            var ad = _context.Ads.Find(adId);
            if(ad != null)
            {
                ad.ViewsCount++;
                _context.Update(ad);
                _context.SaveChanges();
            }
        }

        [HttpPost]
        [Route("[controller]/[action]")]
        public IActionResult Upload([FromHeader] string jsonString)
        {
            var ad = JsonSerializer.Deserialize<Ad>(jsonString);
            Ad newAd = new Ad(ad);
            _context.Add(newAd);
            _context.SaveChanges();

            return Ok();
        }

        [HttpGet]
        [Route("[controller]/[action]/{agentId}")]
        public List<Ad> Get(string agentId)
        {
            var result = _context.Ads.Where(x => x.AgentId == agentId).ToList();

            return result;
        }

        [HttpGet]
        [Route("[controller]/[action]/{agentId}")]
        public List<Ad> GetForUser(int age)
        {
            return _adsService.GetForUser(age);
        }
    }
}
