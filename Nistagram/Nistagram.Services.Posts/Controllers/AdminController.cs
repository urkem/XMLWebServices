﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nistagram.Services.Posts.Data;
using Nistagram.Services.Posts.Data.Model;
using Nistagram.Services.Posts.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Controllers
{
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly ILogger<AdminController> _logger;
        private readonly IFileService _fileService;
        private readonly ApplicationDbContext _context;

        public AdminController(ILogger<AdminController> logger, ApplicationDbContext context, IFileService fileService)
        {
            _logger = logger;
            _context = context;
            _fileService = fileService;
        }

        [HttpPost]
        [Route("[controller]/[action]/")]
        public async Task<IActionResult> Upload([FromForm] string userId, [FromForm] IFormFile[] files)
        {
            VerificationRequest request = new VerificationRequest();

            List<string> localUrls = new List<string>();
            List<string> webUrls = await _fileService.UploadFileToCloudinary(files);

            foreach (var file in files)
            {
                localUrls.Append(await _fileService.UploadFileToLocal(file));
            }

            if(localUrls.Count == 1)
            {
                request.LocalUrl = localUrls.First();
                request.WebUrl = localUrls.First();
            }
            else
            {
                request.LocalUrl = JsonSerializer.Serialize(localUrls);
                request.WebUrl = JsonSerializer.Serialize(webUrls);
            }

            request.UserId = userId;
            request.IsVerified = false;
            //request.ExtractedText = await _fileService.ExtractTextFromImageAsync(request.ImageUrl);
            request.ExtractedText = "Turn image-to-text on for this feature.";

            _context.VerificationRequests.Add(request);

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet]
        [Route("[controller]/[action]")]
        public List<VerificationRequest> GetAllRequests()
        {
            var allRequests = _context.VerificationRequests.ToList();

            return allRequests;
        }

        [HttpGet]
        [Route("[controller]/[action]/{userId}")]
        public VerificationRequest GetByUserId(string userId)
        {
            return _context.VerificationRequests.Where(x => x.UserId == userId).FirstOrDefault();
        }

        [HttpPost]
        [Route("[controller]/[action]/{userId}")]
        public IActionResult VerifyUserById(string userId)
        {
            var request = _context.VerificationRequests.Where(x => x.UserId == userId).FirstOrDefault();

            if (request != null)
            {
                request.IsVerified = true;

                _context.SaveChanges();
            }

            return Ok();
        }
    }
}
