﻿using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data
{
    public class CommentsDTO
    {
        public CommentsDTO()
        {

        }

        public CommentsDTO(Comment x)
        {
            this.Content = x.Content;
            this.UserId = x.UserId;
            this.TimeOfCommenting = x.TimeOfCommenting;
        }

        public string Content { get; set; }
        public string UserId { get; set; }
        public DateTime TimeOfCommenting { get; set; }
    }
}
