﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data
{
    public class NotificationDTO
    {
        public NotificationDTO()
        {

        }

        public NotificationDTO(string userId, string userNotify, string contentType, string content, DateTime timeOfSending)
        {
            UserId = userId;
            UserNotify = userNotify;
            ContentType = contentType;
            Content = content;
            TimeOfSending = timeOfSending;
        }

        public string UserId { get; set; }
        public string UserNotify { get; set; }
        public string ContentType { get; set; }
        public string Content { get; set; }
        public DateTime TimeOfSending { get; set; }
    }
}
