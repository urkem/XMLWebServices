﻿using Nistagram.Services.Posts.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data
{
    public class PostsDTO : Post
    {
        public List<string> Urls { get; set; }
        public new long NumberOfLikes { get; set; }
        public new long NumberOfDislikes { get; set; }
        public new long NumberOfComments { get; set; }
        public new DateTime TimeOfCreation { get; set; }
        public string TargetUri { get; set; }
        public bool IsAd { get; set; }

        public PostsDTO(Post post) : base(post)
        {
            //this.Urls = JsonSerializer.Deserialize<List<string>>(this.LocalUrl);
            this.NumberOfLikes = post.NumberOfLikes;
            this.NumberOfDislikes = post.NumberOfDislikes;
            this.NumberOfComments = post.NumberOfComments;
            this.TimeOfCreation = post.TimeOfCreation;
        }

        public PostsDTO(Ad ad)
        {
            this.Description = ad.TargetUrl;
            this.WebUrl = ad.ImageUrl;
            this.UserId = ad.Name;
            this.IsAd = true; 
        }
    }
}
