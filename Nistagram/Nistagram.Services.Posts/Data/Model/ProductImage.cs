﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data.Model
{
    [Table("tbProductImages")]
    public class ProductImage
    {
        [Key]
        public int ProductId { get; private set; }

        public byte[] Image { get; set; }
    }
}
