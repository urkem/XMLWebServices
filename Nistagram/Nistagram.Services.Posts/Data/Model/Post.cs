﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;

namespace Nistagram.Services.Posts.Data.Model
{
    [Table("tbPosts")]
    public class Post
    {
        public Post()
        {
            NumberOfLikes = 0;
            NumberOfDislikes = 0;
            NumberOfComments = 0;
        }

        public Post(Post post)
        {
            this.Id = post.Id;
            this.LocalUrl = post.LocalUrl;
            this.WebUrl = post.WebUrl;
            this.UserId = post.UserId;
            this.Tags = post.Tags;
            this.Description = post.Description;
            this.NumberOfLikes = post.NumberOfDislikes;
            this.NumberOfDislikes = post.NumberOfDislikes;
            this.NumberOfComments = post.NumberOfComments;
        }

        [Key]
        public Guid Id { get; set; }
        public string LocalUrl { get; set; }
        public string WebUrl { get; set; }
        public string UserId { get; set; }
        public DateTime TimeOfCreation { get; set; }
        public string Tags { get; set; }
        public bool IsPrivate { get; set; }
        public string Description { get; set; }
        public string ProfileUserId { get; set; }
        public long NumberOfLikes { get; set; }
        public long NumberOfDislikes { get; set; }
        public long NumberOfComments { get; set; }
    }
}
