﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data.Model
{
    [Table("tbReactions")]
    public class Reaction
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("tbPost")]
        public Guid PostId { get; set; }

        public bool IsLiked { get; set; }

        public string UserId { get; set; }

    }
}
