﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data.Model
{
    [Table("tbVerificationRequests")]
    public class VerificationRequest
    {
        public Guid Id { get; set; }
        public string UserId { get; set; }
        public string LocalUrl { get; set; }
        public string WebUrl { get; set; }
        public bool IsVerified { get; set; }
        public DateTime CreationTime { get; private set; }
        public string ExtractedText { get; set; } //Text extracted from image

        public VerificationRequest()
        {
            CreationTime = DateTime.Now;
        }
    }
}
