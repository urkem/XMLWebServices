﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data.Model
{
    [Table("tbComments")]
    public class Comment
    {
        [Key]
        public Guid Id { get; set; }

        [ForeignKey("tbPost")]
        public Guid PostId { get; set; }

        public string Content { get; set; }

        public string UserId { get; set; }

        public DateTime TimeOfCommenting { get; set; }

    }
}
