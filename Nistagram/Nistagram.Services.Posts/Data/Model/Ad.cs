﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Services.Posts.Data.Model
{
    public class Ad
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string TargetUrl { get; set; }
        public string ImageUrl { get; set; }
        public string AgentId { get; set; }
        public int ViewsCount { get; set; }

        public Ad()
        {
            ViewsCount = 0;
        }

        public Ad(Ad ad) : base()
        {
            this.AgentId = ad.AgentId;
            this.End = ad.End;
            this.Start = ad.Start;
            this.MaxAge = ad.MaxAge;
            this.MinAge = ad.MinAge;
            this.Name = ad.Name;
            this.TargetUrl = ad.TargetUrl;
            this.ImageUrl = ad.ImageUrl;
        }
    }
}
