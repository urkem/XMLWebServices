﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
//using System.Data.Entity;
using System.Collections.Generic;
using System.Text;
using Nistagram.Agents.Models;
using Microsoft.AspNetCore.Identity;

namespace Nistagram.Agents.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public Microsoft.EntityFrameworkCore.DbSet<Nistagram.Agents.Models.Item> Item { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Nistagram.Agents.Models.Buyer> Buyer { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Nistagram.Agents.Models.Cart> Cart { get; set; }
        public Microsoft.EntityFrameworkCore.DbSet<Nistagram.Agents.Models.CartItem> CartItem { get; set; }
        public DbSet<Nistagram.Agents.Models.Promotion> Promotion { get; set; }
    }
}
