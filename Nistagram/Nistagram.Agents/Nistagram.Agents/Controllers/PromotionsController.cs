﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Nistagram.Agents.Data;
using Nistagram.Agents.Models;
using Nistagram.Agents.Models.DTO;

namespace Nistagram.Agents.Controllers
{
    public class PromotionsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<IdentityUser> _userManager; 
        private IHostingEnvironment _env;
        //private readonly HttpClient _httpClient;
        private string _nistagramApiPost = "https://localhost:5006/Marketing/Upload/";
        private string _nistagramApiGet = "https://localhost:5006/Marketing/Get/";

        public PromotionsController(ApplicationDbContext context, 
            UserManager<IdentityUser> userManager, IHostingEnvironment env)
            //, HttpClient client)
        {
            _context = context;
            _env = env;
            _userManager = userManager;
            //_httpClient = client;
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Promotions
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Promotion.Where(x => x.AgentId == user.Id).ToListAsync());
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Promotions/Details/5
        public async Task<IActionResult> Send(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promotion = await _context.Promotion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (promotion == null)
            {
                return NotFound();
            }

            string json = JsonConvert.SerializeObject(promotion);
            HttpClient client = new HttpClient();

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, _nistagramApiPost);
            request.Headers.Add("jsonString", json);

            request.Content = new StringContent("OK", Encoding.UTF8, "text/plain");

            HttpResponseMessage response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> SaveToXML(int id)
        {
            var promotions = new List<PromotionDTO>();
            var user = await _userManager.GetUserAsync(User);
            string url = _nistagramApiGet + user.Id;

            // usually you create on HttpClient per Application (it is the best practice)
            HttpClient client = new HttpClient();

            using (HttpResponseMessage response = client.GetAsync(url).Result)
            {
                using (HttpContent content = response.Content)
                {
                    var json = content.ReadAsStringAsync().Result;
                    promotions = JsonConvert.DeserializeObject<List<PromotionDTO>>(json);
                }
            }

            var promotion = promotions.Where(x => x.Id == id).FirstOrDefault();
            if (promotion == null)
                return NotFound();

            var extension = ".xml";
            var fileName = DateTime.Now.Ticks + extension; //Create a new Name for the file due to security reasons.

            var pathBuilt = Path.Combine(Directory.GetCurrentDirectory(), "Upload\\files");

            if (!Directory.Exists(pathBuilt))
            {
                Directory.CreateDirectory(pathBuilt);
            }

            XmlSerializer xs = new XmlSerializer(typeof(PromotionDTO));

            TextWriter txtWriter = new StreamWriter(pathBuilt + "\\" + fileName);

            var fileFullPath = pathBuilt + "\\" + fileName;

            xs.Serialize(txtWriter, promotion);

            txtWriter.Close();

            var encodedPath = fileFullPath.Replace(":\\", "~~~").Replace("\\", "~~");

            using (HttpResponseMessage response = client.PostAsync("http://localhost:5000/api/" + encodedPath, null).Result)
            {
                using (HttpContent content = response.Content)
                {
                    var json = content.ReadAsStringAsync().Result;
                }
            }

            return RedirectToAction("ViewPromotions");
        }

        public async Task<IActionResult> ViewPromotions()
        {
            var promotions = new List<PromotionDTO>();
            var user = await _userManager.GetUserAsync(User);
            string url = _nistagramApiGet + user.Id;

            // usually you create on HttpClient per Application (it is the best practice)
            HttpClient client = new HttpClient();
            
            using (HttpResponseMessage response = client.GetAsync(url).Result)
            {
                using (HttpContent content = response.Content)
                {
                    var json = content.ReadAsStringAsync().Result;
                    promotions = JsonConvert.DeserializeObject<List<PromotionDTO>>(json);
                }
            }
            return View(promotions);
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Promotions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promotion = await _context.Promotion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (promotion == null)
            {
                return NotFound();
            }

            ViewData["json"] = JsonConvert.SerializeObject(promotion);

            return View(promotion);
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Promotions/Create
        public async Task<IActionResult> Create(int id)
        {
            var user = await _userManager.GetUserAsync(User);

            var item = _context.Item.Find(id);
            if (item == null)
                return NotFound();

            ViewData["imageUrl"] = item.WebImageUri;

            Promotion promotion = new Promotion();
            promotion.AgentId = user.Id;
            promotion.Start = DateTime.Now;
            promotion.End = DateTime.Now;
            promotion.ImageUrl = item.WebImageUri;
            promotion.TargetUrl = "https://localhost:44339/Items/Details/" + id.ToString();

            return View(promotion);
        }

        // POST: Promotions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> Create([Bind("Id,Name,Start,End,MinAge,MaxAge,TargetUrl,ImageUrl,AgentId")] Promotion promotion)
        {
            if (ModelState.IsValid)
            {
                Promotion p = new Promotion();
                p.AgentId = promotion.AgentId;
                p.End = promotion.End;
                p.Start = promotion.Start;
                p.ImageUrl = promotion.ImageUrl;
                p.MaxAge = promotion.MaxAge;
                p.MinAge = promotion.MinAge;
                p.Name = promotion.Name;
                p.TargetUrl = promotion.TargetUrl;

                _context.Add(p);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(promotion);
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Promotions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promotion = await _context.Promotion.FindAsync(id);
            if (promotion == null)
            {
                return NotFound();
            }
            return View(promotion);
        }

        // POST: Promotions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Start,End,MinAge,MaxAge,TargetUrl,ImageUrl,AgentId")] Promotion promotion)
        {
            if (id != promotion.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(promotion);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PromotionExists(promotion.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(promotion);
        }

        // GET: Promotions/Delete/5
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promotion = await _context.Promotion
                .FirstOrDefaultAsync(m => m.Id == id);
            if (promotion == null)
            {
                return NotFound();
            }

            return View(promotion);
        }

        // POST: Promotions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var promotion = await _context.Promotion.FindAsync(id);
            _context.Promotion.Remove(promotion);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PromotionExists(int id)
        {
            return _context.Promotion.Any(e => e.Id == id);
        }
    }
}
