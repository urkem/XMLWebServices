﻿using System;
using System.Web;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Nistagram.Agents.Data;
using Nistagram.Agents.Models;
using Nistagram.Agents.Models.DTO;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Identity;

namespace Nistagram.Agents.Controllers
{
    public class ItemsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly Cloudinary _cloudinary;
        private readonly UserManager<IdentityUser> _userManager;

        public ItemsController(ApplicationDbContext context, 
            Cloudinary cloudinary, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _cloudinary = cloudinary;
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Items
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Item
                        .Where(x => x.AgentId == user.Id)
                        .ToListAsync());
        }

        //[Authorize(Policy = "BuyerPolicy")]
        // GET: Items
        public async Task<IActionResult> Shop()
        {
            return View(await _context.Item.ToListAsync());
        }

        [Authorize(Policy = "BuyerPolicy")]
        public async Task<IActionResult> PreviousCarts()
        {
            var user = await _userManager.GetUserAsync(User);

            Buyer buyer;
            if (_context.Buyer.Where(x => x.UserId == user.Id).Count() == 0)
            {
                buyer = new Buyer();
                buyer.UserId = user.Id;
                _context.Add(buyer);
                _context.SaveChanges();
            }
            else
            {
                buyer = _context.Buyer.Where(x => x.UserId == user.Id).FirstOrDefault();
            }

            var carts = _context.Cart
                .Where(x => x.BuyerId == buyer.Id && x.Id != buyer.CurrentCart)
                .ToList();

            return View("Carts", carts);
        }

        [Authorize(Policy = "BuyerPolicy")]
        public async Task<IActionResult> ViewCart(int id)
        {
            var cart = _context.Cart.Find(id);

            var itemIds = _context.CartItem
                .Where(x => x.CartId == cart.Id)
                .Select(x => x.ItemId)
                .ToList();

            var items = new List<Item>();

            foreach (var itemId in itemIds)
            {
                items.Add(_context.Item.Find(itemId));
            }

            return View(items);
        }

        [Authorize(Policy = "BuyerPolicy")]
        public async Task<IActionResult> Checkout()
        {
            var user = await _userManager.GetUserAsync(User);

            Buyer buyer;
            if (_context.Buyer.Where(x => x.UserId == user.Id).Count() == 0)
            {
                buyer = new Buyer();
                buyer.UserId = user.Id;
                _context.Add(buyer);
                _context.SaveChanges();
            }
            else
            {
                buyer = _context.Buyer.Where(x => x.UserId == user.Id).FirstOrDefault();
            }

            Cart cart;
            cart = new Cart();
            cart.BuyerId = buyer.Id;
            _context.Add(cart);
            _context.SaveChanges();
            buyer.CurrentCart = cart.Id;
            _context.Update(buyer);
            _context.SaveChanges();

            return View("Shop", await _context.Item.ToListAsync());
        }

        [Authorize(Policy = "BuyerPolicy")]
        public async Task<IActionResult> MyCart()
        {
            var user = await _userManager.GetUserAsync(User);

            Buyer buyer;
            if (_context.Buyer.Where(x => x.UserId == user.Id).Count() == 0)
            {
                buyer = new Buyer();
                buyer.UserId = user.Id;
                _context.Add(buyer);
                _context.SaveChanges();
            }
            else
            {
                buyer = _context.Buyer.Where(x => x.UserId == user.Id).FirstOrDefault();
            }

            Cart cart;

            if (buyer.CurrentCart == null || _context.Cart.Find(buyer.CurrentCart) == null)
            {
                cart = new Cart();
                cart.BuyerId = buyer.Id;
                _context.Add(cart);
                _context.SaveChanges();
                buyer.CurrentCart = cart.Id;
                _context.Update(buyer);
                _context.SaveChanges();
            }
            else
            {
                cart = _context.Cart.Find(buyer.CurrentCart);
            }

            var itemIds = _context.CartItem
                .Where(x => x.CartId == cart.Id)
                .Select(x => x.ItemId)
                .ToList();

            var items = new List<Item>();

            foreach (var itemId in itemIds)
            {
                items.Add(_context.Item.Find(itemId)); 
            }

            return View(items);
        }

        // GET: Items/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Item
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // GET: Items/Details/5
        public async Task<IActionResult> DeleteFromCart(int? id)
        {
            var user = await _userManager.GetUserAsync(User);

            Buyer buyer;
            if (_context.Buyer.Where(x => x.UserId == user.Id).Count() == 0)
            {
                buyer = new Buyer();
                buyer.UserId = user.Id;
                _context.Add(buyer);
                _context.SaveChanges();
            }
            else
            {
                buyer = _context.Buyer.Where(x => x.UserId == user.Id).FirstOrDefault();
            }

            int l = 0;
            //removes only first element with this id
            var cart = _context.Cart.Find(buyer.CurrentCart);
            var cartItem = _context.CartItem
                            .Where(x => x.ItemId == id && x.CartId == cart.Id)
                            .FirstOrDefault();
            if(cartItem != null)
            {
                _context.Remove(cartItem);
                _context.SaveChanges();
            }

            var itemIds = _context.CartItem
                .Where(x => x.CartId == cart.Id)
                .Select(x => x.ItemId)
                .ToList();

            var items = new List<Item>();

            foreach (var itemId in itemIds)
            {
                items.Add(_context.Item.Find(itemId));
            }

            return View("MyCart", items);
        }

        [HttpGet]
        [Authorize(Policy = "BuyerPolicy")]
        public async Task<IActionResult> AddToCart(int id)
        {
            var itemId = id;
            var user = await _userManager.GetUserAsync(User);

            if (user == null)
            {
                return NotFound();
            }
            if(_context.Item.Find(itemId) == null)
            {
                return NotFound();
            }

            var item = await _context.Item
                .FirstOrDefaultAsync(m => m.Id == itemId);

            if (item == null)
            {
                return NotFound();
            }

            Buyer buyer;
            if(_context.Buyer.Where(x => x.UserId == user.Id).Count() == 0)
            {
                buyer = new Buyer();
                buyer.UserId = user.Id;
                _context.Add(buyer);
                _context.SaveChanges();
            }
            else
            {
                buyer = _context.Buyer.Where(x => x.UserId == user.Id).FirstOrDefault();
            }

            Cart cart;
            if(buyer.CurrentCart == null || _context.Cart.Find(buyer.CurrentCart) == null)
            {
                cart = new Cart();
                cart.BuyerId = buyer.Id;
                _context.Add(cart);
                _context.SaveChanges();
                buyer.CurrentCart = cart.Id;
                _context.Update(buyer);
                _context.SaveChanges();
            }
            else
            {
                cart = _context.Cart.Find(buyer.CurrentCart);
            }

            CartItem ca = new CartItem();
            ca.CartId = cart.Id;
            ca.ItemId = item.Id;

            _context.Add(ca);
            _context.SaveChanges();

            return View("Shop", await _context.Item.ToListAsync());
        }


        [Authorize(Policy = "AgentPolicy")]
        // GET: Items/Create
        public IActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Upload(int id)
        {
            ViewData["id"] = id;
            return View(new UploadFileDTO());
        }

        [HttpPost]
        public async Task<IActionResult> Upload(UploadFileDTO model)
        {
            if (model.MyImage != null)
            {
                if(_context.Item.Where(x => x.Id == model.ImageId).Count() == 0)
                {
                    return NotFound();
                }

                var item = _context.Item.Find(model.ImageId);

                var link = await UploadFileToCloudinary(model.MyImage);

                item.WebImageUri = link;

                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return RedirectToAction("Index", "Home");
        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);
        }

        // POST: Items/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,LocalImageUri,WebImageUri,Price,AgentId,CreationTime,UploadedFile")] ItemDTO item)
        {
            var user = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                item.AgentId = user.Id;
                _context.Add(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        [Authorize(Policy = "AgentPolicy")]
        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["id"] = id;
            var item = await _context.Item.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "AgentPolicy")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,LocalImageUri,WebImageUri,Price,AgentId,CreationTime")] Item item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(item);
        }

        // GET: Items/Delete/5
        [Authorize(Policy = "AgentPolicy,AdminPolicy")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Item
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var item = await _context.Item.FindAsync(id);
            _context.Item.Remove(item);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemExists(int id)
        {
            return _context.Item.Any(e => e.Id == id);
        }

        private async Task<string> UploadFileToCloudinary(IFormFile file)
        {
            var result = await _cloudinary.UploadAsync(new ImageUploadParams
            {
                File = new FileDescription(file.FileName,
                        file.OpenReadStream()),
                Tags = "nistagram_album"
            }).ConfigureAwait(false);

            return result.Url.ToString();
        }
    }
}
