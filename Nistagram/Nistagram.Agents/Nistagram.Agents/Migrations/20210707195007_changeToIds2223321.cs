﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Agents.Migrations
{
    public partial class changeToIds2223321 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BuyerId",
                table: "Cart",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cart_BuyerId",
                table: "Cart",
                column: "BuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Buyer_BuyerId",
                table: "Cart",
                column: "BuyerId",
                principalTable: "Buyer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cart_Buyer_BuyerId",
                table: "Cart");

            migrationBuilder.DropIndex(
                name: "IX_Cart_BuyerId",
                table: "Cart");

            migrationBuilder.DropColumn(
                name: "BuyerId",
                table: "Cart");
        }
    }
}
