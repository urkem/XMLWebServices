﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Nistagram.Agents.Migrations
{
    public partial class newMigs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cart_Buyer_BuyerId",
                table: "Cart");

            migrationBuilder.DropForeignKey(
                name: "FK_Item_Cart_CartId",
                table: "Item");

            migrationBuilder.DropIndex(
                name: "IX_Item_CartId",
                table: "Item");

            migrationBuilder.DropIndex(
                name: "IX_Cart_BuyerId",
                table: "Cart");

            migrationBuilder.DropColumn(
                name: "CartId",
                table: "Item");

            migrationBuilder.AlterColumn<int>(
                name: "BuyerId",
                table: "Cart",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CartItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CartId = table.Column<int>(nullable: false),
                    ItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CartItem", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CartItem");

            migrationBuilder.AddColumn<int>(
                name: "CartId",
                table: "Item",
                type: "int",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "BuyerId",
                table: "Cart",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateIndex(
                name: "IX_Item_CartId",
                table: "Item",
                column: "CartId");

            migrationBuilder.CreateIndex(
                name: "IX_Cart_BuyerId",
                table: "Cart",
                column: "BuyerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cart_Buyer_BuyerId",
                table: "Cart",
                column: "BuyerId",
                principalTable: "Buyer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Item_Cart_CartId",
                table: "Item",
                column: "CartId",
                principalTable: "Cart",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
