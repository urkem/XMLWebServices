﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string LocalImageUri { get; set; }
        public string WebImageUri { get; set; }
        public double Price { get; set; }
        public string AgentId { get; set; }
        public DateTime CreationTime { get; private set; }
        public Item()
        {
            CreationTime = DateTime.Now;
            WebImageUri = "https://i.pinimg.com/originals/ae/8a/c2/ae8ac2fa217d23aadcc913989fcc34a2.png";
        }
    }
}
