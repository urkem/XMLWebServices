﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models
{
    public class Buyer
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int CurrentCart { get; set; }

        public Buyer()
        {
        }

        public Buyer(string userId) : base()
        {
            UserId = userId;
        }
    }
}
