﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models
{
    public class Cart
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; private set; }
        public int BuyerId { get; set; }

        public Cart()
        {
            CreationTime = DateTime.Now;
        }
    }
}
