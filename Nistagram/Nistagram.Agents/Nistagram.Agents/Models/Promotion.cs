﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models
{
    public class Promotion
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string TargetUrl { get; set; }
        public string ImageUrl { get; set; }
        public string AgentId { get; set; }
    }
}
