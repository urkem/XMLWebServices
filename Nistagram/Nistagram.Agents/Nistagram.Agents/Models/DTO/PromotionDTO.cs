﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models.DTO
{
    public class PromotionDTO : Promotion
    {
        public int ViewsCount { get; set; }
    }
}
