﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nistagram.Agents.Models.DTO
{
    public class ItemDTO : Item
    {
        public IFormFile UploadedFile { get; set; }
    }
}
