using System;

namespace Nistagram.Common
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}