﻿using System;

namespace Nistagram.Common.Contracts
{
    public record TestSenderItemCreated(Guid ItemId, string Message);
        
    public record TestSenderItemUpdated(Guid ItemId, string Message);
        
    public record TestSenderItemDeleted(Guid ItemId);   

    public record SendIdToGetPost(Guid guid);
}
