﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Nistagram.Common.Settings;

namespace Nistagram.Common.MassTransit
{
    public static class Extensions
    {
        public static IServiceCollection AddMassTransitWithRabbitMq(this IServiceCollection services)
        {
            services.AddMassTransit(configure =>
            {
                configure.AddConsumers(Assembly.GetEntryAssembly());

                configure.UsingRabbitMq((context, configutaor)
                =>
                {
                    var configuration = context.GetService<IConfiguration>();
                    
                    //var serviceSettings = configuration.GetSection(nameof(ServiceSettings)).Get<ServiceSettings>();
                    //var rabbitMQSettings = configuration.GetSection(nameof(RabbitMQSettings)).Get<RabbitMQSettings>();
                    
                    configutaor.Host("rabbitmq");
                    configutaor.ConfigureEndpoints(context, new KebabCaseEndpointNameFormatter("catalog", false));
                });
            });

            services.AddMassTransitHostedService();

            return services;
        }
    }
}
